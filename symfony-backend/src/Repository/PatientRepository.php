<?php

namespace App\Repository;

use App\Entity\Patient;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query\ResultSetMapping;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Patient|null find($id, $lockMode = null, $lockVersion = null)
 * @method Patient|null findOneBy(array $criteria, array $orderBy = null)
 * @method Patient[]    findAll()
 * @method Patient[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PatientRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Patient::class);
    }

    public function findByFirstLetter($entityManager, string $letter)
    {
        $conn = $entityManager->getConnection();

        $sql = 'SELECT * FROM patient WHERE last_name LIKE :letter ORDER BY last_name ASC';
        $stmt = $conn->prepare($sql);
        $stmt->execute(['letter' => $letter]);

        return $stmt->fetchAll();
    }
}
