<?php

namespace App\Repository;

use App\Entity\Appointment;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Appointment|null find($id, $lockMode = null, $lockVersion = null)
 * @method Appointment|null findOneBy(array $criteria, array $orderBy = null)
 * @method Appointment[]    findAll()
 * @method Appointment[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AppointmentRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Appointment::class);
    }

    public function findByDate($entityManager, string $date)
    {
        $conn = $entityManager->getConnection();

        $start_date = \DateTime::createFromFormat('Y/m', $date)
            ->modify('first day of this month')->format('Y/m/d');

        $end_date = \DateTime::createFromFormat('Y/m', $date)
            ->modify('first day of this month')->modify('+1 month')->format('Y/m/d');

        $sql = '
            SELECT *
            FROM appointment
            WHERE status = "Pending" AND
            date BETWEEN :start_date AND :end_date
            ORDER BY date ASC
        ';
        $stmt = $conn->prepare($sql);
        $stmt->execute(['start_date' => $start_date, 'end_date' => $end_date]);

        return $stmt->fetchAll();
    }

    public function findTodaysAppointments($entityManager)
    {
        $conn = $entityManager->getConnection();

        $today = date('Y-m-d');
        $tomorrow = date('Y-m-d', strtotime('+1 day'));

        $sql = '
            SELECT *
            FROM appointment
            WHERE date BETWEEN :today and :tomorrow
            ORDER BY date ASC
        ';
        $stmt = $conn->prepare($sql);
        $stmt->execute(['today' => $today, 'tomorrow' => $tomorrow]);

        return $stmt->fetchAll();
    }
}
