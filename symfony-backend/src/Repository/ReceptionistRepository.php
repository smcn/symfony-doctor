<?php

namespace App\Repository;

use App\Entity\Receptionist;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Receptionist|null find($id, $lockMode = null, $lockVersion = null)
 * @method Receptionist|null findOneBy(array $criteria, array $orderBy = null)
 * @method Receptionist[]    findAll()
 * @method Receptionist[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ReceptionistRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Receptionist::class);
    }

//    /**
//     * @return Receptionist[] Returns an array of Receptionist objects
//     */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('r.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Receptionist
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
