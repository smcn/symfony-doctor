<?php

namespace App\Controller;

use App\Controller\ApiController;
use App\Entity\Patient;
use App\Helper\CreateUserHelper;
use App\Helper\LoginHelper;
use App\Security\AuthKey;
use App\Security\Sanitizer;
use App\Security\Validator;
use App\Repository\PatientRepository;
use App\Repository\AppointmentRepository;
use App\Repository\PrescriptionRepository;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\ORM\EntityManagerInterface;

class PatientController extends ApiController
{
    /**
     * @Route("/patients", name="patient_create", methods={"POST"})
     */
    public function createPatient(Request $request, EntityManagerInterface $entityManager, AuthKey $authKey)
    {
        $authToken = $request->headers->get('authorization') ?? '';

        if ($this->authenticate($authToken, ['Receptionist'])) {
            $headers = ['authorization' => $authKey->updateToken($authToken)];
            $allowedData = [
                'firstName'   => 'string',
                'lastName'    => 'string',
                'password'    => 'password',
                'dateOfBirth' => 'date'
            ];

            $validator = new Validator($request->request->all(), $allowedData, $entityManager);
            if ($validator->validate()) {
                $sanitizer = new Sanitizer($request->request->all(), $allowedData);
                $sanitizedData = $sanitizer->sanitize();

                $createUserHelper = new CreateUserHelper('App\Entity\Patient', $sanitizedData, $entityManager);
                $username = $createUserHelper->saveUserToDatabase();
                return $this->respond(['username' => $username], 201, $headers);
            } else {
                return $this->respondWithErrors($validator->getErrors(), 400, $headers);
            }
        } else {
            return $this->respondUnauthorized();
        }
    }

    /**
     * @Route("/patients/{uuid}", name="patient_show", methods={"GET"})
     */
    public function showPatient(
        Request $request,
        PatientRepository $patientRepo,
        AuthKey $authKey,
        string $uuid
    ) {
        $authToken = $request->headers->get('authorization') ?? '';

        if ($this->authenticate($authToken, ['All'])) {
            $headers = ['authorization' => $authKey->updateToken($authToken)];

            try {
                $patient = $patientRepo->find($uuid);
                $data = [
                    'firstName'   => $patient->getFirstName(),
                    'lastName'    => $patient->getLastName(),
                    'dateOfBirth' => $patient->getDateOfBirth()->format('Y-m-d')
                ];
                return $this->respond($data, 200, $headers);
            } catch (\Exception $e) {
                return $this->respondWithErrors(['Patient could not be found'], 400, $headers);
            }
        } else {
            return $this->respondUnauthorized();
        }
    }

    /**
     * @Route("/patients", name="patients_show_all", methods={"GET"})
     */
    public function showAllPatients(
        Request $request,
        PatientRepository $patientRepo,
        AuthKey $authKey,
        EntityManagerInterface $entityManager
    ) {
        $authToken = $request->headers->get('authorization') ?? '';

        if ($this->authenticate($authToken, ['Receptionist'])) {
            $headers = ['authorization' => $authKey->updateToken($authToken)];

            $formattedArrayOfPatients = [];

            $letter = $request->query->get('letter') ?? '';
            if ($letter !== '') {
                if (sizeof(str_split($letter)) > 1) {
                    $letter = str_split($letter)[0];
                }

                $letter .= '%';
                $patients = $patientRepo->findByFirstLetter($entityManager, $letter);

                $arrLen = sizeof($patients);
                for ($i = 0; $i < $arrLen; $i++) {
                    $formattedArrayOfPatients[$patients[$i]['id']] =
                        $patients[$i]['first_name'] .' '. $patients[$i]['last_name'];
                };
            } else {
                $patients = $patientRepo->findBy([], ['username' => 'ASC']);

                foreach ($patients as $patient) {
                    $formattedArrayOfPatients[(string)$patient->getId()] =
                        $patient->getFirstName() .' '. $patient->getLastName();
                }
            }

            return $this->respond($formattedArrayOfPatients, 200, $headers);
        } else {
            return $this->respondUnauthorized();
        }
    }

    /**
     * @Route("/patients/{uuid}/appointments", name="patient_appointments", methods={"GET"})
     */
    public function getAppointments(
        Request $request,
        AppointmentRepository $appRepo,
        PatientRepository $patientRepo,
        AuthKey $authKey,
        string $uuid
    ) {
        $authToken = $request->headers->get('authorization') ?? '';

        if ($this->authenticate($authToken, ['Receptionist', 'Patient'])) {
            $headers = ['authorization' => $authKey->updateToken($authToken)];

            // this ensures that patients can only view their own prescriptions
            $decryptedAuthToken = unserialize(base64_decode($authToken));
            if ($decryptedAuthToken['userClass'] === 'Patient') {
                $uuid = $decryptedAuthToken['userUuid'];
            }

            try {
                $allAppointments = $patientRepo->find($uuid)->getAppointments();
            } catch (\Exception $e) {
                return $this->respondWithErrors(["Patient could not be found"], 400, $headers);
            }

            $sortedAppointments = [];
            foreach ($allAppointments as $appointment) {
                $doctor = $appointment->getDoctor();

                $sortedAppointments[$appointment->getId()] = [
                    'doctor' => [
                        'id'        => $doctor->getId(),
                        'firstName' => $doctor->getFirstName(),
                        'lastName'  => $doctor->getLastName()
                    ],
                    'status' => $appointment->getStatus(),
                    'time'   => $appointment->getDate(),
                    'note'   => $appointment->getNote()
                ];
            }

            return $this->respond($sortedAppointments, 200, $headers);
        } else {
            return $this->respondUnauthorized();
        }
    }

    /**
     * @Route("/patients/{uuid}/prescriptions", name="patient_prescriptions", methods={"GET"})
     */
    public function getPrescriptions(
        Request $request,
        PrescriptionRepository $prescriptionRepo,
        PatientRepository $patientRepo,
        AuthKey $authKey,
        string $uuid
    ) {
        $authToken = $request->headers->get('authorization') ?? '';

        if ($this->authenticate($authToken, ['Doctor', 'Patient'])) {
            $headers = ['authorization' => $authKey->updateToken($authToken)];

            // this ensures that patients can only view their own prescriptions
            $decryptedAuthToken = unserialize(base64_decode($authToken));
            if ($decryptedAuthToken['userClass'] === 'Patient') {
                $uuid = $decryptedAuthToken['userUuid'];
            }

            try {
                $allPrescriptions = $patientRepo->find($uuid)->getPrescriptions();
            } catch (\Exception $e) {
                return $this->respondWithErrors(["Patient could not be found"], 400, $headers);
            }

            $sortedPrescriptions = [];
            foreach ($allPrescriptions as $prescription) {
                $patient = $prescription->getPatient();

                $sortedPrescriptions[(string)$prescription->getId()] = [
                    'patient' => [
                        'id'        => $patient->getId(),
                        'firstName' => $patient->getFirstName(),
                        'lastName'  => $patient->getLastName()
                    ],
                    'medication' => $prescription->getMedication()
                ];
            }

            return $this->respond($sortedPrescriptions, 200, $headers);
        } else {
            return $this->respondUnauthorized();
        }
    }

    /**
     * @Route("/patients/login", name="patient_login", methods={"POST"})
     */
    public function login(Request $request, PatientRepository $patientRepo)
    {
        $loginHelper = new LoginHelper($request->request->all(), $patientRepo);

        if ($loginHelper->validate()) {
            $data = $loginHelper->login();
            return $this->respond($data['data'], 200, $data['headers']);
        } else {
            $errors = $loginHelper->getErrors();
            return $this->respondWithErrors($errors, 400);
        }
    }
}
