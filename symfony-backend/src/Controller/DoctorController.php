<?php

namespace App\Controller;

use App\Controller\ApiController;
use App\Entity\Doctor;
use App\Helper\CreateUserHelper;
use App\Helper\LoginHelper;
use App\Security\AuthKey;
use App\Security\Sanitizer;
use App\Security\Validator;
use App\Repository\AppointmentRepository;
use App\Repository\DoctorRepository;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\ORM\EntityManagerInterface;

class DoctorController extends ApiController
{
    /**
     * @Route("/doctors", name="doctor_create", methods={"POST"})
     */
    public function createDoctor(Request $request, EntityManagerInterface $entityManager, AuthKey $authKey)
    {
        $authToken = $request->headers->get('authorization') ?? '';

        if ($this->authenticate($authToken, ['Receptionist'])) {
            $headers = ['authorization' => $authKey->updateToken($authToken)];
            $allowedData = [
                'firstName'   => 'string',
                'lastName'    => 'string',
                'password'    => 'password',
                'dateOfBirth' => 'date'
            ];

            $validator = new Validator($request->request->all(), $allowedData, $entityManager);
            if ($validator->validate()) {
                $sanitizer = new Sanitizer($request->request->all(), $allowedData);
                $sanitizedData = $sanitizer->sanitize();

                $createUserHelper = new CreateUserHelper('App\Entity\Doctor', $sanitizedData, $entityManager);
                $username = $createUserHelper->saveUserToDatabase();
                return $this->respond(['username' => $username], 201, $headers);
            } else {
                return $this->respondWithErrors($validator->getErrors(), 400, $headers);
            }
        } else {
            return $this->respondUnauthorized();
        }
    }

    /**
     * @Route("/doctors/{uuid}", name="doctor_show", methods={"GET"})
     */
    public function showDoctor(
        Request $request,
        DoctorRepository $doctorRepo,
        AuthKey $authKey,
        string $uuid
    ) {
        $authToken = $request->headers->get('authorization') ?? '';

        if ($this->authenticate($authToken, ['All'])) {
            $headers = ['authorization' => $authKey->updateToken($authToken)];

            try {
                $doctor = $doctorRepo->find($uuid);
                $data = [
                    'firstName'   => $doctor->getFirstName(),
                    'lastName'    => $doctor->getLastName(),
                    'dateOfBirth' => $doctor->getDateOfBirth()->format('Y-m-d')
                ];
                return $this->respond($data, 200, $headers);
            } catch (\Exception $e) {
                return $this->respondWithErrors(['Doctor could not be found'], 400, $headers);
            }
        } else {
            return $this->respondUnauthorized();
        }
    }

    /**
     * @Route("/doctors", name="doctors_show_all", methods={"GET"})
     */
    public function showAllDoctors(
        Request $request,
        DoctorRepository $doctorRepo,
        AuthKey $authKey
    ) {
        $authToken = $request->headers->get('authorization') ?? '';

        if ($this->authenticate($authToken, ['Receptionist', 'Patient'])) {
            $headers = ['authorization' => $authKey->updateToken($authToken)];

            $doctors = $doctorRepo->findBy([], ['username' => 'ASC']);

            $formattedArrayOfDoctors = [];
            foreach ($doctors as $doctor) {
                $formattedArrayOfDoctors[(string)$doctor->getId()] =
                    $doctor->getFirstName() .' '. $doctor->getLastName();
            };

            return $this->respond($formattedArrayOfDoctors, 200, $headers);
        } else {
            return $this->respondUnauthorized();
        }
    }

    /**
     * @Route("/doctors/{uuid}/appointments", name="doctor_appointments", methods={"GET"})
     */
    public function getAppointments(
        Request $request,
        AppointmentRepository $appRepo,
        DoctorRepository $doctorRepo,
        AuthKey $authKey,
        string $uuid
    ) {
        $authToken = $request->headers->get('authorization') ?? '';

        if ($this->authenticate($authToken, ['Receptionist', 'Doctor'])) {
            $headers = ['authorization' => $authKey->updateToken($authToken)];

            // this ensures that doctors can only view their own appointments
            $decryptedAuthToken = unserialize(base64_decode($authToken));
            if ($decryptedAuthToken['userClass'] === 'Doctor') {
                $uuid = $decryptedAuthToken['userUuid'];
            }

            $allAppointments = $doctorRepo->find($uuid)->getAppointments();

            $sortedAppointments = [];
            foreach ($allAppointments as $appointment) {
                $patient = $appointment->getPatient();

                $sortedAppointments[$appointment->getId()] = [
                    'patient' => [
                        'id'        => $patient->getId(),
                        'firstName' => $patient->getFirstName(),
                        'lastName'  => $patient->getLastName()
                    ],
                    'status' => $appointment->getStatus(),
                    'time'   => $appointment->getDate(),
                    'note'   => $appointment->getNote()
                ];
            }

            return $this->respond($sortedAppointments, 200, $headers);
        } else {
            return $this->respondUnauthorized();
        }
    }

    /**
     * @Route("/doctors/login", name="doctor_login", methods={"POST"})
     */
    public function login(Request $request, DoctorRepository $doctorRepo)
    {
        $loginHelper = new LoginHelper($request->request->all(), $doctorRepo);

        if ($loginHelper->validate()) {
            $data = $loginHelper->login();
            return $this->respond($data['data'], 200, $data['headers']);
        } else {
            $errors = $loginHelper->getErrors();
            return $this->respondWithErrors($errors, 400);
        }
    }
}
