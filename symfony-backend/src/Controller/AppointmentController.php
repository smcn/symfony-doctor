<?php

namespace App\Controller;

use App\Controller\ApiController;
use App\Helper\CreateAppointmentHelper;
use App\Repository\AppointmentRepository;
use App\Repository\DoctorRepository;
use App\Repository\PatientRepository;
use App\Security\AuthKey;
use App\Security\Validator;
use App\Security\Sanitizer;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;

class AppointmentController extends ApiController
{
    /**
     * @Route("/appointments", name="appointment_create", methods={"POST"})
     */
    public function createAppointment(Request $request, EntityManagerInterface $entityManager, AuthKey $authKey)
    {
        $authToken = $request->headers->get('authorization') ?? '';

        if ($this->authenticate($authToken, ['Receptionist', 'Patient'])) {
            $headers = ['authorization' => $authKey->updateToken($authToken)];
            $allowedData = [
                'doctor'  => 'doctor',
                'patient' => 'patient',
                'time'    => 'date',
                'note'    => 'nullableString'
            ];

            $requestData = $request->request->all();

            // if a patient tries to create an appointment, its own id is inserted into the request array
            $decrypedToken = unserialize(base64_decode($authToken));
            if ($decrypedToken['userClass'] === 'Patient') {
                $requestData['patient'] = $decrypedToken['userUuid'];
            }

            $validator = new Validator($requestData, $allowedData, $entityManager);
            if ($validator->validate()) {
                $sanitizer = new Sanitizer($requestData, $allowedData);
                $sanitizedData = $sanitizer->sanitize();

                $appointmentHelper = new CreateAppointmentHelper($sanitizedData, $entityManager);
                if ($appointmentHelper->createAppointment()) {
                    return $this->respond(['Appointment created'], 201, $headers);
                } else {
                    return $this->respondWithErrors(['Appointment not created'], 400, $headers);
                }
            } else {
                return $this->respondWithErrors($validator->getErrors(), 400, $headers);
            }
        } else {
            return $this->respondUnauthorized();
        }
    }

    /**
     * @Route("/appointments/today", name="appointments_today", methods={"GET"})
     */
    public function showTodaysAppointments(
        Request $request,
        AuthKey $authKey,
        AppointmentRepository $appRepo,
        DoctorRepository $doctorRepo,
        PatientRepository $patientRepo,
        EntityManagerInterface $entityManager
    ) {
        $authToken = $request->headers->get('authorization') ?? '';

        if ($this->authenticate($authToken, ['Receptionist'])) {
            $headers = ['authorization' => $authKey->updateToken($authToken)];

            $orderedAppointments = [];
            $todaysAppointments = $appRepo->findTodaysAppointments($entityManager);

            $arrLen = sizeof($todaysAppointments);
            for ($i = 0; $i < $arrLen; $i++) {
                $doctor = $doctorRepo->find($todaysAppointments[$i]['doctor_id']);
                $patient = $patientRepo->find($todaysAppointments[$i]['patient_id']);

                $orderedAppointments[$todaysAppointments[$i]['id']] = [
                    'patient' => [
                        'firstName' => $patient->getFirstName(),
                        'lastName'  => $patient->getLastName(),
                    ],
                    'doctor' => [
                        'firstName' => $doctor->getFirstName(),
                        'lastName'  => $doctor->getLastName()
                    ],
                    'time' => $todaysAppointments[$i]['date'],
                    'note' => $todaysAppointments[$i]['note']
                ];
            }

            return $this->respond($orderedAppointments, 200, $headers);
        } else {
            return $this->respondUnauthorized();
        }
    }

    /**
     * @Route("/appointments/{uuid}", name="appointment_update", methods={"PUT"})
     */
    public function updateAppointment(
        Request $request,
        EntityManagerInterface $entityManager,
        AppointmentRepository $appRepo,
        AuthKey $authKey,
        string $uuid
    ) {
        $authToken = $request->headers->get('authorization') ?? '';

        if ($this->authenticate($authToken, ['Receptionist'])) {
            $headers = ['authorization' => $authKey->updateToken($authToken)];

            $requestData = $request->request->all();

            try {
                $appointment = $appRepo->find($uuid);
                $allowedStatus = ['Cancelled', 'Missed', 'Completed', 'Pending', 'Rescheduled'];

                if (!in_array($requestData['status'], $allowedStatus)) {
                    return $this->respondWithErrors(['Status not allowed'], 400, $headers);
                }

                $appointment->setStatus($requestData['status']);
                $entityManager->persist($appointment);
                $entityManager->flush();

                return $this->respond(['Appointment updated'], 201, $headers);
            } catch (\Exception $e) {
                return $this->respondWithErrors(['Appointment could not be found'], 400, $headers);
            }
        } else {
            return $this->respondUnauthorized();
        }
    }

    /**
     * @Route("/appointments/{uuid}", name="appointment_show", methods={"GET"})
     */
    public function showAppointment(
        Request $request,
        AppointmentRepository $appRepo,
        DoctorRepository $doctorRepo,
        PatientRepository $patientRepo,
        AuthKey $authKey,
        string $uuid
    ) {
        $authToken = $request->headers->get('authorization') ?? '';

        if ($this->authenticate($authToken, ['All'])) {
            $headers = ['authorization' => $authKey->updateToken($authToken)];

            try {
                $appointment = $appRepo->find($uuid);

                $patient = $patientRepo->find($appointment->getPatient());
                $doctor = $doctorRepo->find($appointment->getDoctor());

                $data = [
                    'patient' => [
                        'id'        => $patient->getId(),
                        'firstName' => $patient->getFirstName(),
                        'lastName'  => $patient->getLastName(),
                    ],
                    'doctor'  => [
                        'id'        => $doctor->getId(),
                        'firstName' => $doctor->getFirstName(),
                        'lastName'  => $doctor->getLastName()
                    ],
                    'status'  => $appointment->getStatus(),
                    'time'    => date_format($appointment->getDate(), 'Y-m-d H:i'),
                    'note'    => $appointment->getNote() ?? ''
                ];
                return $this->respond($data, 200, $headers);
            } catch (\Exception $e) {
                return $this->respondWithErrors(['Appointment could not be found'], 400, $headers);
            }
        } else {
            return $this->respondUnauthorized();
        }
    }

    /**
     * @Route("/appointments", name="appointments_show_all", methods={"GET"})
     */
    public function showAllAppointments(
        Request $request,
        AppointmentRepository $appointmentRepo,
        PatientRepository $patientRepo,
        DoctorRepository $doctorRepo,
        AuthKey $authKey,
        EntityManagerInterface $entityManager
    ) {
        $authToken = $request->headers->get('authorization') ?? '';

        if ($this->authenticate($authToken, ['Receptionist'])) {
            $headers = ['authorization' => $authKey->updateToken($authToken)];

            $sortedAppointments = [];

            $month = $request->query->get('month') ?? '';
            if ($month !== '') {
                $patientAppointments = $appointmentRepo->findByDate($entityManager, $month);

                $arrLen = sizeof($patientAppointments);
                for ($i = 0; $i < $arrLen; $i++) {
                    $doctor = $doctorRepo->find($patientAppointments[$i]['doctor_id']);
                    $patient = $patientRepo->find($patientAppointments[$i]['patient_id']);

                    $sortedAppointments[$patientAppointments[$i]['id']] = [
                        'doctor'  => [
                            'id'        => $doctor->getId(),
                            'firstName' => $doctor->getFirstName(),
                            'lastName'  => $doctor->getLastName()
                        ],
                        'patient' => [
                            'id'        => $patient->getId(),
                            'firstName' => $patient->getFirstName(),
                            'lastName'  => $patient->getLastName()
                        ],
                        'status' => $patientAppointments[$i]['status'],
                        'time'   => $patientAppointments[$i]['date'],
                        'note'   => $patientAppointments[$i]['note']
                    ];
                }
            } else {
                $pendingAppointments = $appointmentRepo->findBy(['status' => 'Pending'], ['date' => 'ASC']);

                foreach ($pendingAppointments as $appointment) {
                    $doctor = $doctorRepo->find($appointment->getDoctor());
                    $patient= $patientRepo->find($appointment->getPatient());

                    $sortedAppointments[(string)$appointment->getId()] = [
                        'doctor'  => [
                            'id'        => $doctor->getId(),
                            'firstName' => $doctor->getFirstName(),
                            'lastName'  => $doctor->getLastName()
                        ],
                        'patient' => [
                            'id'        => $patient->getId(),
                            'firstName' => $patient->getFirstName(),
                            'lastName'  => $patient->getLastName()
                        ],
                        'status'  => $appointment->getStatus(),
                        'time'    => $appointment->getDate(),
                        'note'    => $appointment->getNote()
                    ];
                };
            }

            return $this->respond($sortedAppointments, 200, $headers);
        } else {
            return $this->respondUnauthorized();
        };
    }
}
