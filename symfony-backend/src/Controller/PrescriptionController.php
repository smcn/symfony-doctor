<?php

namespace App\Controller;

use App\Controller\ApiController;
use App\Helper\CreatePrescriptionHelper;
use App\Repository\PatientRepository;
use App\Repository\PrescriptionRepository;
use App\Security\AuthKey;
use App\Security\Validator;
use App\Security\Sanitizer;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;

class PrescriptionController extends ApiController
{
    /**
     * @Route("/prescriptions", name="prescription_create", methods={"POST"})
     */
    public function createPrescription(Request $request, EntityManagerInterface $entityManager, AuthKey $authKey)
    {
        $authToken = $request->headers->get('authorization') ?? '';

        if ($this->authenticate($authToken, ['Doctor'])) {
            $headers = ['authorization' => $authKey->updateToken($authToken)];
            $allowedData = [
                'patient'    => 'patient',
                'medication' => 'string'
            ];

            $requestData = $request->request->all();
            $validator = new Validator($requestData, $allowedData, $entityManager);

            if ($validator->validate()) {
                $sanitizer = new Sanitizer($requestData, $allowedData);
                $sanitizedData = $sanitizer->sanitize();

                $prescriptionHelper = new CreatePrescriptionHelper($sanitizedData, $entityManager);
                if ($prescriptionHelper->createPrescription()) {
                    return $this->respond(['Prescription created'], 201, $headers);
                } else {
                    return $this->respondWithErrors(['Prescription not created'], 400, $headers);
                }
            } else {
                return $this->respondWithErrors($validator->getErrors(), 400, $headers);
            }
        } else {
            return $this->respondUnauthorized();
        }
    }

    /**
     * @Route("/prescriptions/{uuid}", name="prescription_show", methods={"GET"})
     */
    public function showPrescription(
        Request $request,
        PrescriptionRepository $prescriptionRepo,
        PatientRepository $patientRepo,
        AuthKey $authKey,
        string $uuid
    ) {
        $authToken = $request->headers->get('authorization') ?? '';

        if ($this->authenticate($authToken, ['Doctor', 'Patient'])) {
            $headers = ['authorization' => $authKey->updateToken($authToken)];

            try {
                $prescription = $prescriptionRepo->find($uuid);

                $patient = $patientRepo->find($prescription->getPatient());

                // this ensures that patients can only view their own prescriptions
                $decrypedToken = unserialize(base64_decode($authToken));
                if ($decrypedToken['userClass'] === 'Patient') {
                    if ($decrypedToken['userUuid'] !== $patient) {
                        return $this->respondUnauthorized();
                    }
                }

                $data = [
                    'patient' => [
                        'id'        => $patient->getId(),
                        'firstName' => $patient->getFirstName(),
                        'lastName'  => $patient->getLastName(),
                    ],
                    'medication'  => $prescription->getMedication()
                ];
                return $this->respond($data, 200, $headers);
            } catch (\Exception $e) {
                return $this->respondWithErrors(['Prescription could not be found'], 400, $headers);
            }
        } else {
            return $this->respondUnauthorized();
        }
    }
}
