<?php

namespace App\Controller;

use App\Controller\ApiController;
use App\Entity\Receptionist;
use App\Helper\CreateUserHelper;
use App\Helper\LoginHelper;
use App\Security\AuthKey;
use App\Security\Sanitizer;
use App\Security\Validator;
use App\Repository\ReceptionistRepository;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\ORM\EntityManagerInterface;

class ReceptionistController extends ApiController
{
    /**
     * @Route("/receptionists", name="receptionist_create", methods={"POST"})
     */
    public function createReceptionist(Request $request, EntityManagerInterface $entityManager, AuthKey $authKey)
    {
        $authToken = $request->headers->get('authorization') ?? '';

        if ($this->authenticate($authToken, ['Receptionist'])) {
            $headers = ['authorization' => $authKey->updateToken($authToken)];
            $allowedData = [
                'firstName'   => 'string',
                'lastName'    => 'string',
                'password'    => 'password',
                'dateOfBirth' => 'date'
            ];

            $validator = new Validator($request->request->all(), $allowedData, $entityManager);
            if ($validator->validate()) {
                $sanitizer = new Sanitizer($request->request->all(), $allowedData);
                $sanitizedData = $sanitizer->sanitize();

                $createUserHelper = new CreateUserHelper('App\Entity\Receptionist', $sanitizedData, $entityManager);
                $username = $createUserHelper->saveUserToDatabase();
                return $this->respond(['username' => $username], 201, $headers);
            } else {
                return $this->respondWithErrors($validator->getErrors(), 400, $headers);
            }
        } else {
            return $this->respondUnauthorized();
        }
    }

    /**
     * @Route("/receptionists/{uuid}", name="receptionist_show", methods={"GET"})
     */
    public function showReceptionist(
        Request $request,
        ReceptionistRepository $receptionistRepo,
        AuthKey $authKey,
        string $uuid
    ) {
        $authToken = $request->headers->get('authorization') ?? '';

        if ($this->authenticate($authToken, ['All'])) {
            $headers = ['authorization' => $authKey->updateToken($authToken)];

            try {
                $receptionist = $receptionistRepo->find($uuid);
                $data = [
                    'firstName'   => $receptionist->getFirstName(),
                    'lastName'    => $receptionist->getLastName(),
                    'dateOfBirth' => $receptionist->getDateOfBirth()->format('Y-m-d')
                ];
                return $this->respond($data, 200, $headers);
            } catch (\Exception $e) {
                return $this->respondWithErrors(['Receptionist could not be found'], 400, $headers);
            }
        } else {
            return $this->respondUnauthorized();
        }
    }

    /**
     * @Route("/receptionists/login", name="receptionist_login", methods={"POST"})
     */
    public function login(Request $request, ReceptionistRepository $receptionistRepo)
    {
        $loginHelper = new LoginHelper($request->request->all(), $receptionistRepo);

        if ($loginHelper->validate()) {
            $data = $loginHelper->login();
            return $this->respond($data['data'], 200, $data['headers']);
        } else {
            $errors = $loginHelper->getErrors();
            return $this->respondWithErrors($errors, 400);
        }
    }
}
