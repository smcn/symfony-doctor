<?php

namespace App\Controller;

use App\Security\AuthKey;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class ApiController extends Controller
{
    /**
     * Returns a JSON response
     */
    protected function respond(array $content, int $statusCode, array $headers = []): JsonResponse
    {
        $data = ['data' => $content];
        return new JsonResponse($data, $statusCode, $headers);
    }

    /**
     * Sets an error message and returns a JSON response
     */
    protected function respondWithErrors(array $errors, int $statusCode, array $headers = []): JsonResponse
    {
        $data = ['errors' => $errors];
        return new JsonResponse($data, $statusCode, $headers);
    }

    /**
     * Returns a 401 Unauthorized HTTP response
     */
    protected function respondUnauthorized(string $message = 'Not authorized!'): JsonResponse
    {
        return $this->respondWithErrors([$message], 401);
    }

    /**
     * Authenticates requests based on the User Class taken from the Authorization token checked against an
     * array of allowed classes, specified in the controller method
     */
    protected function authenticate(string $authToken, array $allowedUsers): bool
    {
        $authKey = new AuthKey;

        try {
            $userClass = unserialize(base64_decode($authToken))['userClass'];
        } catch (\Exception $e) {
            return false;
        }

        if (!in_array('All', $allowedUsers)) {
            if (!in_array($userClass, $allowedUsers)) {
                return false;
            }
        }

        // Checks if Authorization token is in the request headers, and then if it's valid
        return null !== $authToken && $authKey->validateToken($authToken);
    }
}
