<?php

namespace App\Security;

class Sanitizer
{
    /**
     * @var array
     */
    private $unsanitizedData;

    /**
     * @var array
     */
    private $expectedData;

    /**
     * @var array
     */
    private $sanitizedArray = [];

    public function __construct(array $unsanitizedData, array $expectedData)
    {
        $this->unsanitizedData = $unsanitizedData;
        $this->expectedData = $expectedData;
    }

    public function sanitize(): array
    {
        foreach ($this->expectedData as $key => $attribute) {
            $sanitizedAttribute = $this->sanitizeAttribute($this->unsanitizedData[$key], $attribute);

            $this->sanitizedArray[$key] = $sanitizedAttribute;
        }

        return $this->sanitizedArray;
    }

    private function sanitizeAttribute($attribute, string $type)
    {
        $attribute = trim(strip_tags($attribute));

        switch ($type) {
            case 'string':
                return $this->filterString($attribute);

            case 'nullableString':
                if (!is_null($attribute)) {
                    return $this->filterString($attribute);
                } else {
                    return true;
                }

            case 'number':
                return filter_var($attribute, FILTER_SANITIZE_NUMBER_INT);

            case 'date':
                return filter_var($attribute, FILTER_SANITIZE_NUMBER_INT);

            default:
                return filter_var($attribute, FILTER_SANITIZE_STRING);
        }
    }

    /**
     * Removes everything that isn't alphabetical, an apostrophe, or whitespace
     */
    private function filterString(string $attribute): string
    {
        $matches = [];
        $str = '';

        preg_match_all("/[a-zA-Z'\s]/", $attribute, $matches);

        foreach ($matches as $match) {
            $str = $str . implode('', $match);
        }

        return $str;
    }
}
