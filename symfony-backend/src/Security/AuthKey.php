<?php

namespace App\Security;

use Carbon\Carbon;
use Predis;

class AuthKey
{
    public function __construct()
    {
        $this->predis = new Predis\Client();
    }

    /**
     * @param string $uuid This is the uuid of an Entity
     *
     * Creates a new API authentication token
     */
    public function createToken(string $uuid, string $class): string
    {
        $token = [
            'time'      => Carbon::now()->toDateString(),
            'tokenId'   => bin2hex(random_bytes(20)),
            'userClass' => $class,
            'userUuid'  => $uuid
        ];

        $token = base64_encode(serialize($token));

        $this->cacheToken($token, $uuid);
        return $token;
    }

    /**
     * Returns true if the API token is valid
     *
     * "Valid" meaning the token matches the one in the cache
     * and $token['time'] is within 30 minutes of creation
     */
    public function validateToken(string $token): bool
    {
        $this->decryptToken($token);
        return $this->tokenIsCorrect($token) && $this->tokenDateIsValid($this->decryptedToken['time']);
    }

    /**
     * Updates time on API token and returns a new token
     */
    public function updateToken(string $token): string
    {
        $this->decryptToken($token);
        $this->decryptedToken['time'] = Carbon::now();
        $newToken = base64_encode(serialize($this->decryptedToken));

        $this->cacheToken($newToken, $this->decryptedToken['userUuid']);
        return $newToken;
    }

    /**
     * Stores the API token in the cache and sets the expire time
     */
    private function cacheToken(string $token, string $uuid): void
    {
        $this->predis->set($uuid . '_auth_token', $token);
        $this->predis->executeRaw(['EXPIRE', $uuid . '_auth_token', 1800]);
    }

    /**
     * Transforms the API token from a string to a PHP array
     */
    private function decryptToken($token): void
    {
        $this->decryptedToken = unserialize(base64_decode($token));
    }

    /**
     * Returns true if the Api token matches the one in the cache
     */
    private function tokenIsCorrect(string $token): bool
    {
        if (isset($this->decryptedToken['userUuid'])) {
            $uuid = $this->decryptedToken['userUuid'];
            $cachedToken = $this->predis->get($uuid . '_auth_token');

            return $cachedToken === $token;
        } else {
            return false;
        }
    }

    /**
     * Returns true if the Api token's date is still valid
     */
    private function tokenDateIsValid(string $time): bool
    {
        if (Carbon::now()->addMinutes(30) > $time) {
            return true;
        } else {
            return false;
        }
    }
}
