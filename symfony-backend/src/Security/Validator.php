<?php

namespace App\Security;

use Carbon\Carbon;
use App\Repository\DoctorRepository;
use App\Repository\PatientRepository;
use Doctrine\ORM\EntityManagerInterface;

class Validator
{
    /**
     * An array of unvalidated data
     *
     * @var array
     */
    private $givenData;

    /**
     * An array of the expected data in $this->givenData, and the expected type
     *
     * @var array
     */
    private $expectedData;

    /**
     * @var Doctrine\ORM\EntityManagerInterface;
     */
    private $entityManager;

    /**
     * @var array $errors;
     */
    private $errors = [];

    public function __construct(array $givenData, array $expectedData, EntityManagerInterface $entityManager = null)
    {
        $this->givenData = $givenData;
        $this->expectedData = $expectedData;
        $this->entityManager = $entityManager;
    }

    /**
     * Iterates through the $expectedData and validates that the array key is in $givenData and that it matches
     * the $expectedData's type
     */
    public function validate(): bool
    {
        foreach ($this->expectedData as $attribute => $type) {
            try {
                $valid = $this->validateAttribute($this->givenData[$attribute], $type);

                if (!$valid) {
                    if (($type !== 'doctor') && ($type !== 'patient')) {
                        array_push($this->errors, $attribute . ' is not of type ' . $type);
                    } else {
                        array_push($this->errors, $type . ' could not be found');
                    }
                }
            } catch (\Exception $e) {
                array_push($this->errors, $attribute . ' is missing');
            }
        }

        if (count($this->errors) > 0) {
            return false;
        } else {
            return true;
        }
    }

    /**
     * Returns the errors array
     */
    public function getErrors(): array
    {
        return $this->errors;
    }

    /**
     * Ensures that the $attribute is of type $type
     */
    private function validateAttribute($attribute, string $type): bool
    {
        switch ($type) {
            case 'doctor':
                return $this->userExists($attribute, 'Doctor');

            case 'patient':
                return $this->userExists($attribute, 'Patient');

            case 'string':
                return is_string($attribute) && !is_numeric($attribute);

            case 'nullableString':
                return is_string($attribute) || is_null($attribute);

            case 'number':
                return is_numeric($attribute);

            case 'date':
                return $this->is_date($attribute);

            case 'password':
                return is_string($attribute);

            default:
                return false;
        }
    }

    /**
     * Uses $this->entityManager to check if the UUID is in the database
     */
    private function userExists(string $uuid, string $class): bool
    {
        try {
            $this->entityManager->getRepository('App\Entity\\' . $class)->find($uuid);
            return true;
        } catch (\Exception $e) {
            return false;
        }
    }

    /**
     * Checks if the $attribute is of type date by trying to create a Carbon object with it
     *
     * The method name isn't camelCase to preserve consistency with the built in type checkers
    */
    private function is_date(string $attribute): bool
    {
        try {
            Carbon::parse($attribute);
            return true;
        } catch (\Exception $e) {
            return false;
        }
    }
}
