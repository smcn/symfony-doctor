<?php

namespace App\Helper;

use App\Security\AuthKey;

class LoginHelper
{
    /**
     * @var array
     */
    private $errors = [];

    /**
     * @var array
     */
    private $content;

    /**
     * @var string
     */
    private $username;

    /**
     * @var string
     */
    private $password;

    /**
     * @var string
     */
    private $user;

    /**
     * @var App\Repository
     */
    private $repository;

    public function __construct(array $content, $repository)
    {
        $this->content = $content;
        $this->repository = $repository;
    }

    /**
     * Validates that a username and password is in the request content. If so, it'll try to find the user,
     * which then leads to the password being verified. If any of these steps fail, it returns false
     */
    public function validate(): bool
    {
        $this->isPresent('username');
        $this->isPresent('password');

        if (isset($this->username) && isset($this->password)) {
            if ($this->findUser()) {
                return $this->validatePassword();
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    /**
     * Logs the user in. This creates an authorization key and returns an array of data to be passed back
     * to the client
     */
    public function login(): array
    {
        $authKey = new AuthKey;
        $class = explode('\\', get_class($this->user))[2];
        $newAuthKey = $authKey->createToken($this->user->getId(), $class);
        return [
            'headers' => ['Authorization' => $newAuthKey],
            'data' => [
                'notifications' => ['Successfully logged in'],
                'user' => [
                    'username'  => $this->user->getUsername(),
                    'uuid'      => $this->user->getId(),
                    'firstName' => $this->user->getFirstName(),
                    'lastName'  => $this->user->getLastName()
                    ]
                ]
            ];
    }

    /**
     * Returns the error array
     */
    public function getErrors(): array
    {
        return $this->errors;
    }

    /**
     * Checks if the $field is present in the request content
     */
    private function isPresent(string $field): bool
    {
        if (isset($this->content[$field])) {
            $this->{$field} = $this->content[$field];
            return true;
        } else {
            array_push($this->errors, "The $field is missing");
            return false;
        }
    }

    /**
     * Uses a user repository to find a user by their username
     */
    private function findUser(): bool
    {
        $user = $this->repository->findOneBy(['username' => $this->username]);
        if ($user) {
            $this->user = $user;
            return true;
        } else {
            array_push($this->errors, 'User not found');
            return false;
        }
    }

    /**
     * Checks the input password against the saved password
     */
    private function validatePassword(): bool
    {
        if (password_verify($this->password, $this->user->getPassword())) {
            return true;
        } else {
            array_push($this->errors, 'Incorrect password');
            return false;
        }
    }
}
