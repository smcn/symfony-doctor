<?php

namespace App\Helper;

use App\Entity\Appointment;
use App\Entity\Doctor;
use App\Entity\Patient;
use Doctrine\ORM\EntityManagerInterface;
use Carbon\Carbon;

class CreateAppointmentHelper
{
    /**
     * @var Doctrine\ORM\EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var App\Entity\Patient
     */
    private $patient;

    /**
     * @var App\Entity\Doctor
     */
    private $doctor;

    /**
     * @var Carbon\Carbon
     */
    private $date;

    /**
     * @var string
     */
    private $note;

    public function __construct(array $input, EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
        $this->patient       = $this->findUser($input['patient'], 'Patient');
        $this->doctor        = $this->findUser($input['doctor'], 'Doctor');
        $this->date          = Carbon::parse($input['time']);
        $this->note          = $input['note'];
    }

    public function createAppointment(): bool
    {
        if (!$this->ensureAppointmentIsValid()) {
            return false;
        }

        try {
            $appointment = new Appointment;
            $appointment->setPatient($this->patient);
            $appointment->setDoctor($this->doctor);
            $appointment->setDate($this->date);
            $appointment->setNote($this->note);
            $appointment->setStatus('Pending');

            $this->entityManager->persist($appointment);
            $this->entityManager->flush();

            return true;
        } catch (\Exception $e) {
            return false;
        }
    }

    private function findUser(string $uuid, string $class): object
    {
        return $this->entityManager->getRepository('App\Entity\\' . $class)->find($uuid);
    }

    private function ensureAppointmentIsValid(): bool
    {
        $possibleAppointments = $this->entityManager->getRepository('App\Entity\Appointment')
        ->findBy(['date' => $this->date]);

        foreach ($possibleAppointments as $appointment) {
            $patient = $appointment->getPatient();
            $doctor  = $appointment->getDoctor();

            if ($doctor->getId() === $this->doctor->getId() or
                $patient->getId() === $this->patient->getId()) {
                return false;
            }
        }

        return true;
    }
}
