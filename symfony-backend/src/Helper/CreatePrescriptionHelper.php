<?php

namespace App\Helper;

use App\Entity\Prescription;
use App\Entity\Patient;
use Doctrine\ORM\EntityManagerInterface;

class CreatePrescriptionHelper
{
    /**
     * @var Doctrine\ORM\EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var App\Entity\Patient
     */
    private $patient;

    /**
     * @var string
     */
    private $medication;

    public function __construct(array $input, EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
        $this->patient       = $this->findUser($input['patient'], 'Patient');
        $this->medication    = $input['medication'];
    }

    public function createPrescription(): bool
    {
        try {
            $prescription = new Prescription;
            $prescription->setPatient($this->patient);
            $prescription->setMedication($this->medication);

            $this->entityManager->persist($prescription);
            $this->entityManager->flush();

            return true;
        } catch (\Exception $e) {
            return false;
        }
    }

    private function findUser(string $uuid, string $class): object
    {
        return $this->entityManager->getRepository('App\Entity\\' . $class)->find($uuid);
    }
}
