<?php

namespace App\Helper;

use App\Entity\Doctor;
use App\Entity\Receptionist;
use App\Entity\Patient;
use Doctrine\ORM\EntityManagerInterface;
use Carbon\Carbon;

class CreateUserHelper
{
    /**
     * A new user, can be either a Receptionist, Doctor, or Patient
     *
     * @var object
     */
    private $user;

    /**
     * @var array
     */
    private $input;

    /**
     * @var string
     */
    private $password;

    /**
     * @var Doctrine\ORM\EntityManagerInterface;
     */
    private $entityManager;

    public function __construct(string $userType, array $input, EntityManagerInterface $entityManager)
    {
        $this->user          = new $userType;
        $this->input         = $input;
        $this->entityManager = $entityManager;
    }

    /**
     * Ensures that the developer has validated and sanitized the input then saves the user to the database
     */
    public function saveUserToDatabase(): string
    {
        $this->createUsername();

        $this->user->setFirstName($this->input['firstName']);
        $this->user->setLastName($this->input['lastName']);
        $this->user->setUsername($this->username);
        $this->user->setDateOfBirth(Carbon::parse($this->input['dateOfBirth']));
        $this->user->setPassword($this->input['password']);

        $this->entityManager->persist($this->user);
        $this->entityManager->flush();

        return $this->username;
    }

    /**
     * Creates the users username. It's simply the first name plus the last name plus the year of their birth
     */
    private function createUsername(): void
    {
        $this->username = $this->input['firstName'] .
                          $this->input['lastName']  .
                          Carbon::parse($this->input['dateOfBirth'])->year;
    }
}
