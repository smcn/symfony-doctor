<?php

namespace App\Tests\PatientControllerTests;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class ShowAppointmentsTest extends WebTestCase
{
    public function setUp()
    {
        $this->client = static::createClient();
    }

    public function testLoginAsPatient()
    {
        $patient = ['username' => 'admin',
                    'password' => 'password'];

        $this->client->request('POST', '/patients/login', $patient);
        $response = $this->client->getResponse();

        $this->assertArrayHasKey('authorization', $response->headers->all());
        return $response->headers->get('Authorization');
    }

    public function testLoginAsReceptionist()
    {
        $receptionist = ['username' => 'admin',
                         'password' => 'symfony-doctor-admin'];

        $this->client->request('POST', '/receptionists/login', $receptionist);
        $response = $this->client->getResponse();

        $this->assertArrayHasKey('authorization', $response->headers->all());
        return $response->headers->get('Authorization');
    }

    /**
     * @depends testLoginAsPatient
     */
    public function testSuccessfulShowAppointmentsAsPatient($authToken)
    {
        $this->client->request(
            'GET',
            '/patients/not-the-uuid/appointments',
            [],
            [],
            ["HTTP_AUTHORIZATION" => $authToken]
        );

        $response = $this->client->getResponse();
        $content  = json_decode($response->getContent(), true);

        $this->assertSame(200, $response->getStatusCode());
        $this->assertNotSame($authToken, $response->headers->get('Authorization'));

        /**
         * The following asserts that the structure of the response is as follows:
         *
         * appointmentId => [
         *     doctor => [
         *         id        => id
         *         firstName => firstName,
         *         lastName  => lastName
         *     ],
         *     status => status,
         *     time   => time,
         *     note   => note
         * ]
         */

        $this->assertArrayHasKey('0b4b7974-dabe-4f94-ab22-939ddc19df5a', $content['data']);
        $this->assertArrayHasKey('doctor', $content['data']['0b4b7974-dabe-4f94-ab22-939ddc19df5a']);
        $this->assertArrayHasKey(
            'id',
            $content['data']['0b4b7974-dabe-4f94-ab22-939ddc19df5a']['doctor']
        );
        $this->assertArrayHasKey(
            'firstName',
            $content['data']['0b4b7974-dabe-4f94-ab22-939ddc19df5a']['doctor']
        );
        $this->assertArrayHasKey(
            'lastName',
            $content['data']['0b4b7974-dabe-4f94-ab22-939ddc19df5a']['doctor']
        );
        $this->assertSame(
            $content['data']['0b4b7974-dabe-4f94-ab22-939ddc19df5a']['doctor']['id'],
            '3025be19-4894-4ba7-bb87-daaf0b611a54'
        );
        $this->assertArrayHasKey('time', $content['data']['0b4b7974-dabe-4f94-ab22-939ddc19df5a']);
        $this->assertArrayHasKey('status', $content['data']['0b4b7974-dabe-4f94-ab22-939ddc19df5a']);
        $this->assertArrayHasKey('note', $content['data']['0b4b7974-dabe-4f94-ab22-939ddc19df5a']);
    }

    /**
     * @depends testLoginAsReceptionist
     */
    public function testShowAppointmentsAsReceptionist($authToken)
    {
        $this->client->request(
            'GET',
            '/patients/ae5157e3-a9ea-42c0-aab5-ceaebccba60c/appointments',
            [],
            [],
            ["HTTP_AUTHORIZATION" => $authToken]
        );

        $response = $this->client->getResponse();
        $content  = json_decode($response->getContent(), true);

        $this->assertSame(200, $response->getStatusCode());
        $this->assertNotSame($authToken, $response->headers->get('Authorization'));

        /**
         * The following asserts that the structure of the response is as follows:
         *
         * appointmentId => [
         *     doctor => [
         *         id        => id
         *         firstName => firstName,
         *         lastName  => lastName
         *     ],
         *     status => status,
         *     time   => time,
         *     note   => note
         * ]
         */

        $this->assertArrayHasKey('0b4b7974-dabe-4f94-ab22-939ddc19df5a', $content['data']);
        $this->assertArrayHasKey('doctor', $content['data']['02005b80-db5b-4fb0-add5-8eee95ed46d2']);
        $this->assertArrayHasKey(
            'id',
            $content['data']['0b4b7974-dabe-4f94-ab22-939ddc19df5a']['doctor']
        );
        $this->assertArrayHasKey(
            'firstName',
            $content['data']['0b4b7974-dabe-4f94-ab22-939ddc19df5a']['doctor']
        );
        $this->assertArrayHasKey(
            'lastName',
            $content['data']['0b4b7974-dabe-4f94-ab22-939ddc19df5a']['doctor']
        );
        $this->assertSame(
            $content['data']['0b4b7974-dabe-4f94-ab22-939ddc19df5a']['doctor']['id'],
            '3025be19-4894-4ba7-bb87-daaf0b611a54'
        );
        $this->assertArrayHasKey('time', $content['data']['0b4b7974-dabe-4f94-ab22-939ddc19df5a']);
        $this->assertArrayHasKey('status', $content['data']['0b4b7974-dabe-4f94-ab22-939ddc19df5a']);
        $this->assertArrayHasKey('note', $content['data']['0b4b7974-dabe-4f94-ab22-939ddc19df5a']);

        return $response->headers->get('Authorization');
    }

    /**
     * @depends testShowAppointmentsAsReceptionist
     */
    public function testUnsuccessfulShowAppointmentsAsReceptionist($authToken)
    {
        $this->client->request(
            'GET',
            '/patients/not-the-uuid/appointments',
            [],
            [],
            ["HTTP_AUTHORIZATION" => $authToken]
        );

        $response = $this->client->getResponse();
        $content  = json_decode($response->getContent(), true);

        $this->assertSame(400, $response->getStatusCode());
        $this->assertNotSame($authToken, $response->headers->get('Authorization'));

        $this->assertSame('Patient could not be found', $content['errors'][0]);
    }

    public function testUnauthorizedShowAppointments()
    {
        $this->client->request('GET', '/patients/ae5157e3-a9ea-42c0-aab5-ceaebccba60c/appointments', [], [], ["HTTP_AUTHORIZATION" => 'not_a_real_auth_token']);

        $response = $this->client->getResponse();
        $content  = json_decode($response->getContent(), true);

        $this->assertSame(401, $response->getStatusCode());
    }
}
