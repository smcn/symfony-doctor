<?php

namespace App\Tests\PatientControllerTests;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class CreateAppointmentTest extends WebTestCase
{
    public function setUp()
    {
        $this->client = static::createClient();
    }

    public function testLoginAsPatient()
    {
        $patient = ['username' => 'testymctesterson1990',
                    'password' => 'password'];

        $this->client->request('POST', '/patients/login', $patient);
        $response = $this->client->getResponse();

        $this->assertArrayHasKey('authorization', $response->headers->all());
        return $response->headers->get('Authorization');
    }

    /**
     * @depends testLoginAsPatient
     */
    public function testSuccessfulCreation($authToken)
    {
        $random_int = random_int(0, 1000);
        $appointmentInfo = [
            'doctor'  => '3025be19-4894-4ba7-bb87-daaf0b611a54',
            'time'    => date('Y-m-d H:i', strtotime("+{$random_int} minutes")),
            'note'    => 'test appointment'
        ];
        $headers = ['HTTP_AUTHORIZATION' => $authToken];

        $this->client->request('POST', '/appointments', $appointmentInfo, [], $headers);
        $response = $this->client->getResponse();

        $this->assertSame($response->getStatusCode(), 201);
    }
}
