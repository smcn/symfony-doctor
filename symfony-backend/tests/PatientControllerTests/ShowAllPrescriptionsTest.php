<?php

namespace App\Tests\PatientControllerTests;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class ShowPrescriptionTest extends WebTestCase
{
    public function setUp()
    {
        $this->client = static::createClient();
    }

    public function testLoginAsPatient()
    {
        $patient = ['username' => 'admin',
                    'password' => 'password'];

        $this->client->request('POST', '/patients/login', $patient);
        $response = $this->client->getResponse();

        $this->assertArrayHasKey('authorization', $response->headers->all());
        return $response->headers->get('Authorization');
    }

    public function testLoginAsDoctor()
    {
        $doctor = ['username' => 'testymctesterson1990',
                  'password' => 'password'];

        $this->client->request('POST', '/doctors/login', $doctor);
        $response = $this->client->getResponse();

        $this->assertArrayHasKey('authorization', $response->headers->all());
        return $response->headers->get('Authorization');
    }

    /**
     * @depends testLoginAsPatient
     */
    public function testSuccessfulShowPrescriptionsAsPatient($authToken)
    {
        $this->client->request(
            'GET',
            '/patients/not-the-uuid/prescriptions',
            [],
            [],
            ["HTTP_AUTHORIZATION" => $authToken]
        );

        $response = $this->client->getResponse();
        $content  = json_decode($response->getContent(), true);

        $this->assertSame(200, $response->getStatusCode());
        $this->assertNotSame($authToken, $response->headers->get('Authorization'));

        /**
         * The following asserts that the structure of the response is as follows:
         *
         * prescriptionId => [
         *     patient => [
         *         id        => id
         *         firstName => firstName,
         *         lastName  => lastName
         *     ],
         *     medication   => note
         * ]
         */

        $this->assertArrayHasKey('38d96c1f-0be7-4383-87af-d871a4162611', $content['data']);
        $this->assertArrayHasKey('patient', $content['data']['38d96c1f-0be7-4383-87af-d871a4162611']);
        $this->assertArrayHasKey('medication', $content['data']['38d96c1f-0be7-4383-87af-d871a4162611']);
        $this->assertArrayHasKey(
            'id',
            $content['data']['38d96c1f-0be7-4383-87af-d871a4162611']['patient']
        );
        $this->assertArrayHasKey(
            'firstName',
            $content['data']['38d96c1f-0be7-4383-87af-d871a4162611']['patient']
        );
        $this->assertArrayHasKey(
            'lastName',
            $content['data']['38d96c1f-0be7-4383-87af-d871a4162611']['patient']
        );
    }

    /**
     * @depends testLoginAsDoctor
     */
    public function testShowPrescriptionsAsDoctor($authToken)
    {
        $this->client->request(
            'GET',
            '/patients/ae5157e3-a9ea-42c0-aab5-ceaebccba60c/prescriptions',
            [],
            [],
            ["HTTP_AUTHORIZATION" => $authToken]
        );

        $response = $this->client->getResponse();
        $content  = json_decode($response->getContent(), true);

        $this->assertSame(200, $response->getStatusCode());
        $this->assertNotSame($authToken, $response->headers->get('Authorization'));

        /**
         * The following asserts that the structure of the response is as follows:
         *
         * prescriptionId => [
         *     patient => [
         *         id        => id
         *         firstName => firstName,
         *         lastName  => lastName
         *     ],
         *     medication   => note
         * ]
         */

        $this->assertArrayHasKey('38d96c1f-0be7-4383-87af-d871a4162611', $content['data']);
        $this->assertArrayHasKey('patient', $content['data']['38d96c1f-0be7-4383-87af-d871a4162611']);
        $this->assertArrayHasKey('medication', $content['data']['38d96c1f-0be7-4383-87af-d871a4162611']);
        $this->assertArrayHasKey(
            'id',
            $content['data']['38d96c1f-0be7-4383-87af-d871a4162611']['patient']
        );
        $this->assertArrayHasKey(
            'firstName',
            $content['data']['38d96c1f-0be7-4383-87af-d871a4162611']['patient']
        );
        $this->assertArrayHasKey(
            'lastName',
            $content['data']['38d96c1f-0be7-4383-87af-d871a4162611']['patient']
        );

        return $response->headers->get('Authorization');
    }

    /**
     * @depends testShowPrescriptionsAsDoctor
     */
    public function testUnsuccessfulShowPrescriptionsAsDoctor($authToken)
    {
        $this->client->request(
            'GET',
            '/patients/not-the-uuid/prescriptions',
            [],
            [],
            ["HTTP_AUTHORIZATION" => $authToken]
        );

        $response = $this->client->getResponse();
        $content  = json_decode($response->getContent(), true);

        $this->assertSame(400, $response->getStatusCode());
        $this->assertNotSame($authToken, $response->headers->get('Authorization'));

        $this->assertSame('Patient could not be found', $content['errors'][0]);
    }

    public function testUnauthorizedShowPrescriptions()
    {
        $this->client->request(
            'GET',
            '/patients/ae5157e3-a9ea-42c0-aab5-ceaebccba60c/prescriptions',
            [],
            [],
            ["HTTP_AUTHORIZATION" => 'not_a_real_auth_token']
        );

        $response = $this->client->getResponse();
        $content  = json_decode($response->getContent(), true);

        $this->assertSame(401, $response->getStatusCode());
    }
}
