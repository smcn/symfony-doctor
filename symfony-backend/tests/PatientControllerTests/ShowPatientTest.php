<?php

namespace App\Tests\PatientControllerTests;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class ShowPatientTest extends WebTestCase
{
    public function setUp()
    {
        $this->client = static::createClient();
    }

    public function testLoginAsPatient()
    {
        $adminPatient = ['username' => 'admin',
                        'password' => 'password'];

        $this->client->request('POST', '/patients/login', $adminPatient);
        $response = $this->client->getResponse();

        $this->assertArrayHasKey('authorization', $response->headers->all());
        return $response->headers->get('Authorization');
    }

    /**
     * @depends testLoginAsPatient
     */
    public function testSuccessfulShowPatient($authToken)
    {
        $this->client->request(
            'GET',
            '/patients/ae5157e3-a9ea-42c0-aab5-ceaebccba60c', // this is the uuid of the admin account
            [],
            [],
            ["HTTP_AUTHORIZATION" => $authToken]
        );

        $response = $this->client->getResponse();
        $content  = json_decode($response->getContent(), true);

        $this->assertSame(200, $response->getStatusCode());
        $this->assertNotSame($authToken, $response->headers->get('Authorization'));

        $this->assertArrayHasKey('firstName', $content['data']);
        $this->assertArrayHasKey('lastName', $content['data']);
        $this->assertArrayHasKey('dateOfBirth', $content['data']);

        $this->assertSame('admin', $content['data']['firstName']);
        $this->assertSame('admin', $content['data']['lastName']);
        $this->assertSame('1700-01-01', $content['data']['dateOfBirth']);

        return $response->headers->get('Authorization');
    }

    /**
     * @depends testSuccessfulShowPatient
     */
    public function testUnsuccessfulShowPatient($authToken)
    {
        $this->client->request(
            'GET',
            '/patients/not-a-real-uuid',
            [],
            [],
            ["HTTP_AUTHORIZATION" => $authToken]
        );

        $response = $this->client->getResponse();
        $content  = json_decode($response->getContent(), true);

        $this->assertSame(400, $response->getStatusCode());
        $this->assertNotSame($authToken, $response->headers->get('Authorization'));

        $this->assertContains('Patient could not be found', $content['errors']);
    }

    public function testUnauthorizedShowPatient()
    {
        $this->client->request(
            'GET',
            '/patients/ae5157e3-a9ea-42c0-aab5-ceaebccba60c', // this is the uuid of the admin account
            [],
            [],
            ["HTTP_AUTHORIZATION" => 'not_a_real_auth_token']
        );

        $response = $this->client->getResponse();
        $content  = json_decode($response->getContent(), true);

        $this->assertSame(401, $response->getStatusCode());
    }
}
