<?php

namespace App\Tests\PatientControllerTests;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class ShowAllPatientTest extends WebTestCase
{
    public function setUp()
    {
        $this->client = static::createClient();
    }

    public function testLoginAsReceptionist()
    {
        $adminReceptionist= ['username' => 'admin',
                             'password' => 'symfony-doctor-admin'];

        $this->client->request('POST', '/receptionists/login', $adminReceptionist);
        $response = $this->client->getResponse();

        $this->assertArrayHasKey('authorization', $response->headers->all());
        return $response->headers->get('Authorization');
    }

    /**
     * @depends testLoginAsReceptionist
     */
    public function testSuccessfulShowAllPatient($authToken)
    {
        $this->client->request('GET', '/patients', [], [], ["HTTP_AUTHORIZATION" => $authToken]);

        $response = $this->client->getResponse();
        $content  = json_decode($response->getContent(), true);

        $this->assertSame(200, $response->getStatusCode());
        $this->assertNotSame($authToken, $response->headers->get('Authorization'));

        $this->assertArrayHasKey('ae5157e3-a9ea-42c0-aab5-ceaebccba60c', $content['data']);
        $this->assertSame($content['data']['ae5157e3-a9ea-42c0-aab5-ceaebccba60c'], 'admin admin');

        return $response->headers->get('Authorization');
    }

    /**
     * @depends testSuccessfulShowAllPatient
     */
    public function testSuccessfulPaginateShowAllPatient($authToken)
    {
        $this->client->request('GET', '/patients?letter=a', [], [], ["HTTP_AUTHORIZATION" => $authToken]);

        $response = $this->client->getResponse();
        $content  = json_decode($response->getContent(), true);

        $this->assertSame(200, $response->getStatusCode());
        $this->assertNotSame($authToken, $response->headers->get('Authorization'));

        $this->assertArrayHasKey('ae5157e3-a9ea-42c0-aab5-ceaebccba60c', $content['data']);
        $this->assertSame($content['data']['ae5157e3-a9ea-42c0-aab5-ceaebccba60c'], 'admin admin');

        return $response->headers->get('Authorization');
    }

    /**
     * @depends testSuccessfulPaginateShowAllPatient
     */
    public function testUnsuccessfulPaginateShowAllPatient($authToken)
    {
        $this->client->request('GET', '/patients?letter=m', [], [], ["HTTP_AUTHORIZATION" => $authToken]);

        $response = $this->client->getResponse();
        $content  = json_decode($response->getContent(), true);

        $this->assertSame(200, $response->getStatusCode());
        $this->assertNotSame($authToken, $response->headers->get('Authorization'));

        $this->assertArrayNotHasKey('ae5157e3-a9ea-42c0-aab5-ceaebccba60c', $content['data']);
    }

    public function testUnauthorizedShowAllPatient()
    {
        $this->client->request('GET', '/patients', [], [], ["HTTP_AUTHORIZATION" => 'not_a_real_auth_token']);

        $response = $this->client->getResponse();
        $content  = json_decode($response->getContent(), true);

        $this->assertSame(401, $response->getStatusCode());
    }
}
