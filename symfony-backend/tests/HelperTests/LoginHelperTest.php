<?php

namespace App\Tests\HelperTests;

use PHPUnit\Framework\TestCase;
use App\Helper\LoginHelper;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\Persistence\ObjectRepository;

class TestUser
{
    public function getUsername()
    {
        return 'testmctesterson1700';
    }

    public function getFirstName()
    {
        return 'test';
    }

    public function getLastName()
    {
        return 'mctesterson';
    }

    public function getId()
    {
        return 'test-uuid';
    }

    public function getPassword()
    {
        return $this->password = password_hash('testpassword', PASSWORD_ARGON2I, ['memory_cost' => 1024, 'time_cost' => 4, 'threads' => 2]);
    }
}

class LoginHelperTest extends TestCase
{
    public function testValidate()
    {
        $content = [
            'username' => 'testmctesterson1700',
            'password' => 'testpassword'
        ];

        $user = new TestUser;

        $userRepo = $this->createMock(ObjectRepository::class);
        $userRepo->expects($this->any())
            ->method('findOneBy')
            ->willReturn($user);

        $loginHelper = new LoginHelper($content, $userRepo);

        $this->assertTrue($loginHelper->validate());

        return [$user, $loginHelper];
    }

    /**
     * @depends testValidate
     */
    public function testLogin($array)
    {
        $user = $array[0];
        $loginHelper = $array[1];

        $content = $loginHelper->login();

        $this->assertArrayHasKey('Authorization', $content['headers']);
        $this->assertSame('Successfully logged in', $content['data']['notifications'][0]);
        $this->assertSame($user->getUsername(), $content['data']['user']['username']);
        $this->assertSame($user->getId(), $content['data']['user']['uuid']);
        $this->assertSame($user->getFirstName(), $content['data']['user']['firstName']);
        $this->assertSame($user->getLastName(), $content['data']['user']['lastName']);
    }
}
