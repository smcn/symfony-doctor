<?php

namespace App\Tests\DoctorControllerTests;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class ShowAppointmentsTest extends WebTestCase
{
    public function setUp()
    {
        $this->client = static::createClient();
    }

    public function testLoginAsDoctor()
    {
        $doctor = ['username' => 'admin',
                   'password' => 'password'];

        $this->client->request('POST', '/doctors/login', $doctor);
        $response = $this->client->getResponse();

        $this->assertArrayHasKey('authorization', $response->headers->all());
        return $response->headers->get('Authorization');
    }

    public function testLoginAsReceptionist()
    {
        $receptionist = ['username' => 'admin',
                         'password' => 'symfony-doctor-admin'];

        $this->client->request('POST', '/receptionists/login', $receptionist);
        $response = $this->client->getResponse();

        $this->assertArrayHasKey('authorization', $response->headers->all());
        return $response->headers->get('Authorization');
    }

    /**
     * @depends testLoginAsDoctor
     */
    public function testSuccessfulShowAppointmentsAsDoctor($authToken)
    {
        $this->client->request(
            'GET',
            '/doctors/not-the-uuid/appointments',
            [],
            [],
            ["HTTP_AUTHORIZATION" => $authToken]
        );

        $response = $this->client->getResponse();
        $content  = json_decode($response->getContent(), true);

        $this->assertSame(200, $response->getStatusCode());
        $this->assertNotSame($authToken, $response->headers->get('Authorization'));

        /**
         * The following asserts that the structure of the response is as follows:
         *
         * appointmentId => [
         *     patient => [
         *         id        => id
         *         firstName => firstName,
         *         lastName  => lastName
         *     ],
         *     status => status,
         *     time   => time,
         *     note   => note
         * ]
         */

        $this->assertArrayHasKey('02005b80-db5b-4fb0-add5-8eee95ed46d2', $content['data']);
        $this->assertArrayHasKey('patient', $content['data']['02005b80-db5b-4fb0-add5-8eee95ed46d2']);
        $this->assertArrayHasKey(
            'id',
            $content['data']['02005b80-db5b-4fb0-add5-8eee95ed46d2']['patient']
        );
        $this->assertArrayHasKey(
            'firstName',
            $content['data']['02005b80-db5b-4fb0-add5-8eee95ed46d2']['patient']
        );
        $this->assertArrayHasKey(
            'lastName',
            $content['data']['02005b80-db5b-4fb0-add5-8eee95ed46d2']['patient']
        );
        $this->assertSame(
            $content['data']['02005b80-db5b-4fb0-add5-8eee95ed46d2']['patient']['id'],
            'ae5157e3-a9ea-42c0-aab5-ceaebccba60c'
        );
        $this->assertArrayHasKey('time', $content['data']['02005b80-db5b-4fb0-add5-8eee95ed46d2']);
        $this->assertArrayHasKey('status', $content['data']['02005b80-db5b-4fb0-add5-8eee95ed46d2']);
        $this->assertArrayHasKey('note', $content['data']['02005b80-db5b-4fb0-add5-8eee95ed46d2']);
    }

    /**
     * @depends testLoginAsReceptionist
     */
    public function testShowAppointmentsAsReceptionist($authToken)
    {
        $this->client->request(
            'GET',
            '/doctors/3025be19-4894-4ba7-bb87-daaf0b611a54/appointments',
            [],
            [],
            ["HTTP_AUTHORIZATION" => $authToken]
        );

        $response = $this->client->getResponse();
        $content  = json_decode($response->getContent(), true);

        $this->assertSame(200, $response->getStatusCode());
        $this->assertNotSame($authToken, $response->headers->get('Authorization'));

        /**
         * The following asserts that the structure of the response is as follows:
         *
         * appointmentId => [
         *     patient => [
         *         id        => id
         *         firstName => firstName,
         *         lastName  => lastName
         *     ],
         *     status => status,
         *     time   => time,
         *     note   => note
         * ]
         */

        $this->assertArrayHasKey('02005b80-db5b-4fb0-add5-8eee95ed46d2', $content['data']);
        $this->assertArrayHasKey('patient', $content['data']['02005b80-db5b-4fb0-add5-8eee95ed46d2']);
        $this->assertArrayHasKey(
            'id',
            $content['data']['02005b80-db5b-4fb0-add5-8eee95ed46d2']['patient']
        );
        $this->assertArrayHasKey(
            'firstName',
            $content['data']['02005b80-db5b-4fb0-add5-8eee95ed46d2']['patient']
        );
        $this->assertArrayHasKey(
            'lastName',
            $content['data']['02005b80-db5b-4fb0-add5-8eee95ed46d2']['patient']
        );
        $this->assertSame(
            $content['data']['02005b80-db5b-4fb0-add5-8eee95ed46d2']['patient']['id'],
            'ae5157e3-a9ea-42c0-aab5-ceaebccba60c'
        );
        $this->assertArrayHasKey('time', $content['data']['02005b80-db5b-4fb0-add5-8eee95ed46d2']);
        $this->assertArrayHasKey('status', $content['data']['02005b80-db5b-4fb0-add5-8eee95ed46d2']);
        $this->assertArrayHasKey('note', $content['data']['02005b80-db5b-4fb0-add5-8eee95ed46d2']);
    }

    public function testUnauthorizedShowAppointments()
    {
        $this->client->request('GET', '/doctors/3025be19-4894-4ba7-bb87-daaf0b611a54/appointments', [], [], ["HTTP_AUTHORIZATION" => 'not_a_real_auth_token']);

        $response = $this->client->getResponse();
        $content  = json_decode($response->getContent(), true);

        $this->assertSame(401, $response->getStatusCode());
    }
}
