<?php

namespace App\Tests\DoctorControllerTests;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class ShowDoctorTest extends WebTestCase
{
    public function setUp()
    {
        $this->client = static::createClient();
    }

    public function testLoginAsDoctor()
    {
        $adminDoctor = ['username' => 'admin',
                        'password' => 'password'];

        $this->client->request('POST', '/doctors/login', $adminDoctor);
        $response = $this->client->getResponse();

        $this->assertArrayHasKey('authorization', $response->headers->all());
        return $response->headers->get('Authorization');
    }

    /**
     * @depends testLoginAsDoctor
     */
    public function testSuccessfulShowDoctor($authToken)
    {
        $this->client->request(
            'GET',
            '/doctors/3025be19-4894-4ba7-bb87-daaf0b611a54', // this is the uuid of the admin account
            [],
            [],
            ["HTTP_AUTHORIZATION" => $authToken]
        );

        $response = $this->client->getResponse();
        $content  = json_decode($response->getContent(), true);

        $this->assertSame(200, $response->getStatusCode());
        $this->assertNotSame($authToken, $response->headers->get('Authorization'));

        $this->assertArrayHasKey('firstName', $content['data']);
        $this->assertArrayHasKey('lastName', $content['data']);
        $this->assertArrayHasKey('dateOfBirth', $content['data']);

        $this->assertSame('admin', $content['data']['firstName']);
        $this->assertSame('admin', $content['data']['lastName']);
        $this->assertSame('1700-01-01', $content['data']['dateOfBirth']);

        return $response->headers->get('Authorization');
    }

    /**
     * @depends testSuccessfulShowDoctor
     */
    public function testUnsuccessfulShowDoctor($authToken)
    {
        $this->client->request(
            'GET',
            '/doctors/not-a-real-uuid',
            [],
            [],
            ["HTTP_AUTHORIZATION" => $authToken]
        );

        $response = $this->client->getResponse();
        $content  = json_decode($response->getContent(), true);

        $this->assertSame(400, $response->getStatusCode());
        $this->assertNotSame($authToken, $response->headers->get('Authorization'));

        $this->assertContains('Doctor could not be found', $content['errors']);
    }

    public function testUnauthorizedShowDoctor()
    {
        $this->client->request(
            'GET',
            '/doctors/3025be19-4894-4ba7-bb87-daaf0b611a54', // this is the uuid of the admin account
            [],
            [],
            ["HTTP_AUTHORIZATION" => 'not_a_real_auth_token']
        );

        $response = $this->client->getResponse();
        $content  = json_decode($response->getContent(), true);

        $this->assertSame(401, $response->getStatusCode());
    }
}
