<?php

namespace App\Tests\DoctorControllerTests;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class CreateDoctorTest extends WebTestCase
{
    public function setUp()
    {
        $this->client = static::createClient();
    }

    public function testLoginAsReceptionist()
    {
        $adminReceptionist = ['username' => 'admin',
                              'password' => 'symfony-doctor-admin'];

        $this->client->request('POST', '/receptionists/login', $adminReceptionist);
        $response = $this->client->getResponse();

        $this->assertArrayHasKey('authorization', $response->headers->all());
        return $response->headers->get('Authorization');
    }

    /**
     * @depends testLoginAsReceptionist
     */
    public function testSuccessfulCreation($authToken)
    {
        $mockUser = [
            'firstName'    => 'testy',
            'lastName'     => 'mctesterson',
            'dateOfBirth'  => '1990-01-01',
            'username'     => 'testymctesterson1990',
            'password'     => 'password'
        ];
        $headers = ['HTTP_AUTHORIZATION' => $authToken];

        $this->client->request('POST', '/doctors', $mockUser, [], $headers);
        $response = $this->client->getResponse();
        $content = json_decode($response->getContent(), true);

        $this->assertSame(201, $response->getStatusCode());
        $this->assertSame($content['data']['username'], $mockUser['username']);

        return $response->headers->get('Authorization');
    }

    /**
     * @depends testSuccessfulCreation
     */
    public function testUnsuccessfulCreation($authToken)
    {
        $mockUser = [];
        $headers = [
            'HTTP_AUTHORIZATION' => $authToken
        ];

        $this->client->request('POST', '/doctors', $mockUser, [], $headers);
        $response = $this->client->getResponse();

        $this->assertSame(400, $response->getStatusCode());
        $this->assertNotSame($response->headers->get('http_authorization'), $authToken);
    }

    public function testUnauthorizedAttempt()
    {
        $this->client->request('POST', '/doctors');
        $response = $this->client->getResponse();

        $this->assertSame(401, $response->getStatusCode());
    }
}
