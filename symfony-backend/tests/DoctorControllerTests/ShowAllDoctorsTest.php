<?php

namespace App\Tests\DoctorControllerTests;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class ShowAllDoctorsTest extends WebTestCase
{
    public function setUp()
    {
        $this->client = static::createClient();
    }

    public function testLoginAsReceptionist()
    {
        $adminReceptionist= ['username' => 'admin',
                             'password' => 'symfony-doctor-admin'];

        $this->client->request('POST', '/receptionists/login', $adminReceptionist);
        $response = $this->client->getResponse();

        $this->assertArrayHasKey('authorization', $response->headers->all());
        return $response->headers->get('Authorization');
    }

    /**
     * @depends testLoginAsReceptionist
     */
    public function testSuccessfulShowAllDoctors($authToken)
    {
        $this->client->request('GET', '/doctors', [], [], ["HTTP_AUTHORIZATION" => $authToken]);

        $response = $this->client->getResponse();
        $content  = json_decode($response->getContent(), true);

        $this->assertSame(200, $response->getStatusCode());
        $this->assertNotSame($authToken, $response->headers->get('Authorization'));

        $this->assertArrayHasKey('3025be19-4894-4ba7-bb87-daaf0b611a54', $content['data']);
        $this->assertSame($content['data']['3025be19-4894-4ba7-bb87-daaf0b611a54'], 'admin admin');
    }

    public function testUnauthorizedShowAllDoctors()
    {
        $this->client->request('GET', '/doctors', [], [], ["HTTP_AUTHORIZATION" => 'not_a_real_auth_token']);

        $response = $this->client->getResponse();
        $content  = json_decode($response->getContent(), true);

        $this->assertSame(401, $response->getStatusCode());
    }
}
