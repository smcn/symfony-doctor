<?php

namespace App\Tests\DoctorControllerTests;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class LoginDoctorTest extends WebTestCase
{
    public function setUp()
    {
        $this->client = static::createClient();
    }

    public function testSuccessfulLogin()
    {
        $doctor = ['username' => 'testymctesterson1990',
                   'password' => 'password'];

        $this->client->request('POST', '/doctors/login', $doctor);
        $response = $this->client->getResponse();
        $content = json_decode($response->getContent(), true);

        $this->assertSame(200, $response->getStatusCode());
        $this->assertArrayHasKey('user', $content['data']);
        $this->assertSame('testymctesterson1990', $content['data']['user']['username']);
        $this->assertSame('testy', $content['data']['user']['firstName']);
        $this->assertSame('mctesterson', $content['data']['user']['lastName']);
    }

    public function testNoUsernameOrPassword()
    {
        $this->client->request('POST', '/doctors/login');
        $response = $this->client->getResponse();
        $content = json_decode($response->getContent(), true);

        $this->assertSame(400, $response->getStatusCode());
        $this->assertContains('The username is missing', $content['errors']);
        $this->assertContains('The password is missing', $content['errors']);
    }

    public function testIncorrectUser()
    {
        $doctor = ['username' => 'not_a_doctor',
                   'password' => 'this is the wrong password'];

        $this->client->request('POST', '/doctors/login', $doctor);
        $response = $this->client->getResponse();
        $content = json_decode($response->getContent(), true);

        $this->assertSame(400, $response->getStatusCode());
        $this->assertContains('User not found', $content['errors']);
    }

    public function testIncorrectPassword()
    {
        $doctor = ['username' => 'testymctesterson1990',
                   'password' => 'this is the wrong password'];

        $this->client->request('POST', '/doctors/login', $doctor);
        $response = $this->client->getResponse();
        $content = json_decode($response->getContent(), true);

        $this->assertSame(400, $response->getStatusCode());
        $this->assertContains('Incorrect password', $content['errors'][0]);
    }
}
