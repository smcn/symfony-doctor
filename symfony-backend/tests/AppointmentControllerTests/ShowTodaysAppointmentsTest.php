<?php

namespace App\Tests\AppointmentControllerTests;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class ShowTodaysAppointmentsTests extends WebTestCase
{
    public function setUp()
    {
        $this->client = static::createClient();
    }

    public function testLoginAsReceptionist()
    {
        $adminReceptionist = ['username' => 'admin',
                              'password' => 'symfony-doctor-admin'];

        $this->client->request('POST', '/receptionists/login', $adminReceptionist);
        $response = $this->client->getResponse();

        $this->assertArrayHasKey('authorization', $response->headers->all());
        return $response->headers->get('Authorization');
    }

    /**
     * Ensures that there is atleast one appointment for today
     *
     * @depends testLoginAsReceptionist
     */
    public function testSuccessfulCreation($authToken)
    {
        $random_int = random_int(0, 1000);
        $appointmentInfo = [
            'doctor'  => '3025be19-4894-4ba7-bb87-daaf0b611a54',
            'patient' => 'ae5157e3-a9ea-42c0-aab5-ceaebccba60c',
            'time'    => date('Y-m-d H:i', strtotime("+{$random_int} minutes")),
            'note'    => 'test appointment'
        ];
        $headers = ['HTTP_AUTHORIZATION' => $authToken];

        $this->client->request('POST', '/appointments', $appointmentInfo, [], $headers);
        $response = $this->client->getResponse();

        $this->assertSame($response->getStatusCode(), 201);

        return $response->headers->get('Authorization');
    }

    /**
     * @depends testSuccessfulCreation
     */
    public function testShowTodaysAppointments($authToken)
    {
        $headers = ['HTTP_AUTHORIZATION' => $authToken];

        $this->client->request('GET', '/appointments/today', [], [], $headers);

        $response = $this->client->getResponse();
        $content = json_decode($response->getContent(), true);

        $this->assertSame($response->getStatusCode(), 200);

        $yesterday = new \DateTime('-1 day');
        $yesterday = $yesterday->format('Y-m-d');
        $tomorrow  = new \DateTime('+1 day');
        $tomorrow = $tomorrow->format('Y-m-d');

        foreach ($content['data'] as $appointment) {
            $appointmentTime = $appointment['time'];
            $appointmentTime = date('Y-m-d', strtotime($appointmentTime));

            $this->assertTrue($appointmentTime > $yesterday);
            $this->assertTrue($appointmentTime < $tomorrow);
            $this->assertFalse($appointmentTime < $yesterday);
            $this->assertFalse($appointmentTime > $tomorrow);
        }
    }

    public function testUnauthorizedAttempt()
    {
        $this->client->request('GET', '/appointments/today');
        $response = $this->client->getResponse();

        $this->assertSame(401, $response->getStatusCode());
    }
}
