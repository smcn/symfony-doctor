<?php

namespace App\Tests\AppointmentControllerTests;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class ShowAllAppointmentsTest extends WebTestCase
{
    public function setUp()
    {
        $this->client = static::createClient();
    }

    public function testLoginAsReceptionist()
    {
        $adminReceptionist= ['username' => 'admin',
                             'password' => 'symfony-doctor-admin'];

        $this->client->request('POST', '/receptionists/login', $adminReceptionist);
        $response = $this->client->getResponse();

        $this->assertArrayHasKey('authorization', $response->headers->all());
        return $response->headers->get('Authorization');
    }

    /**
     * @depends testLoginAsReceptionist
     */
    public function testSuccessfulShowAllAppointments($authToken)
    {
        $this->client->request('GET', '/appointments', [], [], ["HTTP_AUTHORIZATION" => $authToken]);

        $response = $this->client->getResponse();
        $content  = json_decode($response->getContent(), true);

        $this->assertSame(200, $response->getStatusCode());
        $this->assertNotSame($authToken, $response->headers->get('Authorization'));

        $this->assertArrayHasKey('093ad72a-4850-45f6-9e8f-27d6c625a8a2', $content['data']);
        $this->assertSame(
            $content['data']['093ad72a-4850-45f6-9e8f-27d6c625a8a2']['doctor']['id'],
            '3025be19-4894-4ba7-bb87-daaf0b611a54'
        );

        $this->assertSame(
            $content['data']['093ad72a-4850-45f6-9e8f-27d6c625a8a2']['patient']['id'],
            '04432030-d36d-46a5-acd2-11eeea845d5a'
        );

        return $response->headers->get('Authorization');
    }

    /**
     * @depends testSuccessfulShowAllAppointments
     */
    public function testSuccessfulPaginateAppointments($authToken)
    {
        $this->client->request('GET', '/appointments?month=3000/01', [], [], ["HTTP_AUTHORIZATION" => $authToken]);

        $response = $this->client->getResponse();
        $content  = json_decode($response->getContent(), true);

        $this->assertSame(200, $response->getStatusCode());
        $this->assertNotSame($authToken, $response->headers->get('Authorization'));

        $this->assertArrayHasKey('093ad72a-4850-45f6-9e8f-27d6c625a8a2', $content['data']);
        $this->assertSame(
            $content['data']['093ad72a-4850-45f6-9e8f-27d6c625a8a2']['doctor']['id'],
            '3025be19-4894-4ba7-bb87-daaf0b611a54'
        );

        $this->assertSame(
            $content['data']['093ad72a-4850-45f6-9e8f-27d6c625a8a2']['patient']['id'],
            '04432030-d36d-46a5-acd2-11eeea845d5a'
        );
    }

    public function testUnauthorizedShowAllAppointments()
    {
        $this->client->request('GET', '/appointments', [], [], ["HTTP_AUTHORIZATION" => 'not_a_real_auth_token']);

        $response = $this->client->getResponse();
        $content  = json_decode($response->getContent(), true);

        $this->assertSame(401, $response->getStatusCode());
    }
}
