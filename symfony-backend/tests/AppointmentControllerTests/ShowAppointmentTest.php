<?php

namespace App\Tests\AppointmentControllerTests;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class ShowAppointmentTest extends WebTestCase
{
    public function setUp()
    {
        $this->client = static::createClient();
    }

    public function testLoginAsReceptionist()
    {
        $adminReceptionist = ['username' => 'admin',
                              'password' => 'symfony-doctor-admin'];

        $this->client->request('POST', '/receptionists/login', $adminReceptionist);
        $response = $this->client->getResponse();

        $this->assertArrayHasKey('authorization', $response->headers->all());
        return $response->headers->get('Authorization');
    }

    /**
     * @depends testLoginAsReceptionist
     */
    public function testSuccessfulShowAppointment($authToken)
    {
        $this->client->request(
            'GET',
            '/appointments/02005b80-db5b-4fb0-add5-8eee95ed46d2',
            [],
            [],
            ["HTTP_AUTHORIZATION" => $authToken]
        );

        $response = $this->client->getResponse();
        $content  = json_decode($response->getContent(), true);

        $this->assertSame(200, $response->getStatusCode());
        $this->assertNotSame($authToken, $response->headers->get('Authorization'));

        $this->assertArrayHasKey('patient', $content['data']);
        $this->assertArrayHasKey('id', $content['data']['patient']);
        $this->assertArrayHasKey('firstName', $content['data']['patient']);
        $this->assertArrayHasKey('lastName', $content['data']['patient']);

        $this->assertSame('ae5157e3-a9ea-42c0-aab5-ceaebccba60c', $content['data']['patient']['id']);
        $this->assertSame('admin', $content['data']['patient']['firstName']);
        $this->assertSame('admin', $content['data']['patient']['lastName']);

        $this->assertArrayHasKey('doctor', $content['data']);
        $this->assertArrayHasKey('id', $content['data']['doctor']);
        $this->assertArrayHasKey('firstName', $content['data']['doctor']);
        $this->assertArrayHasKey('lastName', $content['data']['doctor']);

        $this->assertSame('3025be19-4894-4ba7-bb87-daaf0b611a54', $content['data']['doctor']['id']);
        $this->assertSame('admin', $content['data']['doctor']['firstName']);
        $this->assertSame('admin', $content['data']['doctor']['lastName']);

        $this->assertArrayHasKey('time', $content['data']);
        $this->assertSame('3000-01-01 09:30', $content['data']['time']);

        $this->assertArrayHasKey('note', $content['data']);
        $this->assertSame('test appointment', $content['data']['note']);

        return $response->headers->get('Authorization');
    }

    /**
     * @depends testSuccessfulShowAppointment
     */
    public function testUnsuccessfulShowAppointment($authToken)
    {
        $this->client->request(
            'GET',
            '/appointments/not-a-real-uuid',
            [],
            [],
            ["HTTP_AUTHORIZATION" => $authToken]
        );

        $response = $this->client->getResponse();
        $content  = json_decode($response->getContent(), true);

        $this->assertSame(400, $response->getStatusCode());
        $this->assertNotSame($authToken, $response->headers->get('Authorization'));

        $this->assertContains('Appointment could not be found', $content['errors']);
    }

    public function testUnauthorizedShowAppointment()
    {
        $this->client->request(
            'GET',
            '/appointments/3025be19-4894-4ba7-bb87-daaf0b611a54', // this is the uuid of the admin account
            [],
            [],
            ["HTTP_AUTHORIZATION" => 'not_a_real_auth_token']
        );

        $response = $this->client->getResponse();
        $content  = json_decode($response->getContent(), true);

        $this->assertSame(401, $response->getStatusCode());
    }
}
