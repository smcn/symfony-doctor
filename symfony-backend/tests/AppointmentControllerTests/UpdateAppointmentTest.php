<?php

namespace App\Tests\AppointmentControllerTests;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class UpdateAppointmentTest extends WebTestCase
{
    public function setUp()
    {
        $this->client = static::createClient();
    }

    public function testLoginAsReceptionist()
    {
        $adminReceptionist = ['username' => 'admin',
                              'password' => 'symfony-doctor-admin'];

        $this->client->request('POST', '/receptionists/login', $adminReceptionist);
        $response = $this->client->getResponse();

        $this->assertArrayHasKey('authorization', $response->headers->all());
        return $response->headers->get('Authorization');
    }

    /**
     * @depends testLoginAsReceptionist
     */
    public function testSuccessfulUpdateAppointment($authToken)
    {
        $this->client->request(
            'PUT',
            '/appointments/02005b80-db5b-4fb0-add5-8eee95ed46d2',
            ['status' => 'Completed'],
            [],
            ["HTTP_AUTHORIZATION" => $authToken]
        );

        $response = $this->client->getResponse();
        $content = json_decode($response->getContent(), true);

        $this->assertSame($response->getStatusCode(), 201);
        $this->assertSame($content['data'][0], 'Appointment updated');

        return $response->headers->get('Authorization');
    }

    /**
     * @depends testSuccessfulUpdateAppointment
     */
    public function testUnsuccessfulUpdateAppointment($authToken)
    {
        $this->client->request(
            'PUT',
            '/appointments/02005b80-db5b-4fb0-add5-8eee95ed46d2',
            ['status' => 'Not allowed'],
            [],
            ["HTTP_AUTHORIZATION" => $authToken]
        );

        $response = $this->client->getResponse();
        $content = json_decode($response->getContent(), true);

        $this->assertSame($response->getStatusCode(), 400);
        $this->assertSame($content['errors'][0], 'Status not allowed');
    }

    public function testUnauthorizedAttempt()
    {
        $this->client->request('PUT', '/appointments/02005b80-db5b-4fb0-add5-8eee95ed46d2');
        $response = $this->client->getResponse();

        $this->assertSame(401, $response->getStatusCode());
    }
}
