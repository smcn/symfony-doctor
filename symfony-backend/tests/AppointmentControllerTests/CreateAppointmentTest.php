<?php

namespace App\Tests\AppointmentControllerTests;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class CreateAppointmentTests extends WebTestCase
{
    public function setUp()
    {
        $this->client = static::createClient();
    }

    public function testLoginAsReceptionist()
    {
        $adminReceptionist = ['username' => 'admin',
                              'password' => 'symfony-doctor-admin'];

        $this->client->request('POST', '/receptionists/login', $adminReceptionist);
        $response = $this->client->getResponse();

        $this->assertArrayHasKey('authorization', $response->headers->all());
        return $response->headers->get('Authorization');
    }

    /**
     * @depends testLoginAsReceptionist
     */
    public function testSuccessfulCreation($authToken)
    {
        $random_int = random_int(0, 1000);
        $appointmentInfo = [
            'doctor'  => '3025be19-4894-4ba7-bb87-daaf0b611a54',
            'patient' => 'ae5157e3-a9ea-42c0-aab5-ceaebccba60c',
            'time'    => date('Y-m-d H:i', strtotime("+{$random_int} minutes")),
            'note'    => 'test appointment'
        ];
        $headers = ['HTTP_AUTHORIZATION' => $authToken];

        $this->client->request('POST', '/appointments', $appointmentInfo, [], $headers);
        $response = $this->client->getResponse();

        $this->assertSame($response->getStatusCode(), 201);

        return $response->headers->get('Authorization');
    }

    /**
     * @depends testSuccessfulCreation
     */
    public function testUnsuccessfulCreation($authToken)
    {
        $appointmentInfo = [];
        $headers = ['HTTP_AUTHORIZATION' => $authToken];

        $this->client->request('POST', '/appointments', $appointmentInfo, [], $headers);
        $response = $this->client->getResponse();
        $content = json_decode($response->getContent(), true);

        $this->assertSame($response->getStatusCode(), 400);
        $this->assertContains('doctor is missing', $content['errors']);
        $this->assertContains('patient is missing', $content['errors']);
        $this->assertContains('time is missing', $content['errors']);

        return $response->headers->get('Authorization');
    }

    /**
     * @depends testUnsuccessfulCreation
     */
    public function testInvalidAttributes($authToken)
    {
        $appointmentInfo = [
            'doctor'  => 'not a real uuid',
            'patient' => 'not a real uuid',
            'time'    => 'not a time',
            'note'    => 17
        ];
        $headers = ['HTTP_AUTHORIZATION' => $authToken];

        $this->client->request('POST', '/appointments', $appointmentInfo, [], $headers);
        $response = $this->client->getResponse();
        $content = json_decode($response->getContent(), true);

        $this->assertSame($response->getStatusCode(), 400);
        $this->assertContains('doctor could not be found', $content['errors']);
        $this->assertContains('patient could not be found', $content['errors']);
        $this->assertContains('time is not of type date', $content['errors']);
        $this->assertContains('note is not of type nullableString', $content['errors']);

        return $response->headers->get('Authorization');
    }

    /**
     * @depends testInvalidAttributes
     */
    public function testAppointmentAlreadyTaken($authToken)
    {
        $appointmentInfo = [
            'doctor'  => '3025be19-4894-4ba7-bb87-daaf0b611a54',
            'patient' => 'ae5157e3-a9ea-42c0-aab5-ceaebccba60c',
            'time'    => '3000-01-01 09:30',
            'note'    => 'test appointment'
        ];
        $headers = ['HTTP_AUTHORIZATION' => $authToken];

        $this->client->request('POST', '/appointments', $appointmentInfo, [], $headers);
        $response = $this->client->getResponse();

        $this->assertSame($response->getStatusCode(), 400);
    }

    public function testUnauthorizedAttempt()
    {
        $this->client->request('POST', '/appointments');
        $response = $this->client->getResponse();

        $this->assertSame(401, $response->getStatusCode());
    }
}
