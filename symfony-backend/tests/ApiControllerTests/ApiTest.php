<?php

namespace App\Tests\ApiControllerTests;

use PHPUnit\Framework\TestCase;
use Symfony\Component\HttpFoundation\JsonResponse;
use App\Controller\ApiController;
use App\Security\AuthKey;

class TestableApiController extends ApiController
{
    /**
     * Creates a public function that exposes ApiController->respond()
     *
     * @return Symfony\Component\HttpFoundation\JsonResponse
     */
    public function testableRespond(array $content, int $statusCode, array $headers = []): JsonResponse
    {
        return $this->respond($content, $statusCode, $headers);
    }

    /**
     * Creates a public function that exposes ApiController->respondWithErrors()
     *
     * @return Symfony\Component\HttpFoundation\JsonResponse
     */
    public function testableRespondWithErrors(array $errors, int $statusCode, array $headers = []): JsonResponse
    {
        return $this->respondWithErrors($errors, $statusCode, $headers);
    }

    /**
     * Creates a public function that exposes ApiController->respondUnauthorized()
     *
     * @return Symfony\Component\HttpFoundation\JsonResponse
     */
    public function testableRespondUnauthorized(string $message = 'Not authorized'): JsonResponse
    {
        return $this->respondUnauthorized($message);
    }

    /**
     * Creates a public function that exposes ApiController->authenticate()
     */
    public function testableAuthenticate(string $authToken, array $allowedUsers): bool
    {
        return $this->authenticate($authToken, $allowedUsers);
    }
}

class ApiTest extends TestCase
{
    public function setUp()
    {
        $this->apiController = new TestableApiController;
    }

    public function testRespond()
    {
        $data = array('Test' => 'Passed');
        $response = $this->apiController->testableRespond($data, 200);
        $statusCode = $response->getStatusCode();
        $content = json_decode($response->getContent(), true);

        $this->assertArrayHasKey('Test', $content['data']);
        $this->assertTrue($statusCode === 200);
        $this->assertTrue($content['data']['Test'] === 'Passed');
    }

    public function testRespondWithErrors()
    {
        $message = array('Huge error');
        $response = $this->apiController->testableRespondWithErrors($message, 404);
        $statusCode = $response->getStatusCode();
        $content = json_decode($response->getContent(), true);

        $this->assertArrayHasKey('errors', $content);
        $this->assertTrue($statusCode === 404);
        $this->assertContains('Huge error', $content['errors']);
    }

    public function testRespondUnauthorized()
    {
        $response = $this->apiController->testableRespondUnauthorized();
        $statusCode = $response->getStatusCode();
        $content = json_decode($response->getContent(), true);

        $this->assertArrayHasKey('errors', $content);
        $this->assertTrue($statusCode === 401);
        $this->assertContains('Not authorized', $content['errors']);
    }

    public function testAuthenticate()
    {
        $authKey = new AuthKey;
        $authToken = $authKey->createToken('this_is_not_real', 'test_class');

        $willPass = ['test_class', 'another_class', 'a_third_class'];
        $wontPass = ['not_test_class', 'neither_is_this_class'];

        $pass = $this->apiController->testableAuthenticate($authToken, $willPass);
        $fail = $this->apiController->testableAuthenticate($authToken, $wontPass);

        $this->assertTrue($pass);
        $this->assertFalse($fail);
    }
}
