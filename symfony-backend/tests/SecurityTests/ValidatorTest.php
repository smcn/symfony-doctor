<?php

namespace App\Tests\SecurityTests;

use PHPUnit\Framework\TestCase;
use App\Security\Validator;
use Doctrine\ORM\EntityManagerInterface;

class ValidatorTests extends TestCase
{
    public function testSuccessfulValidation()
    {
        $givenData = [
            'firstName'       => 'joe',
            'lastName'        => 'smith',
            'favouriteNumber' => 17,
            'birthday'        => '1990-01-01'
        ];

        $expectedData = [
            'firstName'       => 'string',
            'lastName'        => 'string',
            'favouriteNumber' => 'number',
            'birthday'        => 'date'
        ];

        $validator = new Validator($givenData, $expectedData);

        $this->assertTrue($validator->validate());
    }

    public function testUnsuccessfulValidation()
    {
        $givenData = [
            'firstName'       => 17,
            'lastName'        => '1000',
            'favouriteNumber' => 'tennis',
            'birthday'        => 'baseball'
        ];

        $expectedData = [
            'firstName'       => 'string',
            'lastName'        => 'string',
            'favouriteColor'  => 'string',
            'favouriteNumber' => 'number',
            'birthday'        => 'date'
        ];

        $validator = new Validator($givenData, $expectedData);

        $this->assertFalse($validator->validate());

        $errors = $validator->getErrors();

        $this->assertContains('firstName is not of type string', $errors);
        $this->assertContains('lastName is not of type string', $errors);
        $this->assertContains('favouriteColor is missing', $errors);
        $this->assertContains('favouriteNumber is not of type number', $errors);
        $this->assertContains('birthday is not of type date', $errors);
    }
}
