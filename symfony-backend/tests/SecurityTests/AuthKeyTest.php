<?php

namespace App\Tests\SecurityTests;

use PHPUnit\Framework\TestCase;
use App\Security\AuthKey;
use Predis;

class AuthKeyTest extends TestCase
{
    private static $predis;
    private static $authKey;

    public static function setUpBeforeClass()
    {
        self::$predis = new Predis\Client();
        self::$authKey = new AuthKey();
    }

    public function testCreateToken()
    {
        $mockUuid = 'this_is_not_real';
        $token = self::$authKey->createToken($mockUuid, 'mock user class');

        $this->assertSame(self::$predis->get($mockUuid . '_auth_token'), $token);

        return $token;
    }

    /**
     * @depends testCreateToken
     */
    public function testValidateTokenWithValidToken($token)
    {
        $this->assertTrue(self::$authKey->validateToken($token));
    }

    public function testValidateTokenWithInvalidToken()
    {
        $invalidToken = base64_encode(serialize('this_token_doesnt_work'));

        $this->assertFalse(self::$authKey->validateToken($invalidToken));
    }

    /**
     * @depends testCreateToken
     */
    public function testDecodeToken($token)
    {
        $decodedToken = unserialize(base64_decode($token));

        $this->assertArrayHasKey('time', $decodedToken);
        $this->assertArrayHasKey('tokenId', $decodedToken);
        $this->assertArrayHasKey('userClass', $decodedToken);
        $this->assertSame($decodedToken['userClass'], 'mock user class');
        $this->assertArrayHasKey('userUuid', $decodedToken);
    }
    /**
     * @depends testCreateToken
     */
    public function testUpdateToken($token)
    {
        $updatedToken = self::$authKey->updateToken($token);

        $this->assertNotSame($token, $updatedToken);
        $this->assertSame(self::$predis->get('this_is_not_real_auth_token'), $updatedToken);
    }

    public static function tearDownAfterClass()
    {
        self::$predis->del('this_is_not_real_auth_token');
    }
}
