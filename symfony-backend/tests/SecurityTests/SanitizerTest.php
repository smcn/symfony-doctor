<?php

namespace App\Tests\SecurityTests;

use PHPUnit\Framework\TestCase;
use App\Security\Sanitizer;

class SanitizerTests extends TestCase
{
    public function testSuccessfulSanitization()
    {
        $givenData = [
            'firstName'       => ' bobby; drop tables \'students:@#~!"£$%^&*()[]{}?,.\|`¬ ',
            'lastName'        => ' tables1434241 ',
            'favouriteNumber' => ' 17ahda ',
            'birthday'        => ' 1990-01-01 may june july ',
            'invalidData'     => ' this shouldn\'t be in the array '
        ];

        $expectedData = [
            'firstName'       => 'string',
            'lastName'        => 'string',
            'favouriteNumber' => 'number',
            'birthday'        => 'date'
        ];

        $expectedReturn = [
            'firstName'       => 'bobby drop tables \'students',
            'lastName'        => 'tables',
            'favouriteNumber' => '17',
            'birthday'        => '1990-01-01'
        ];

        $sanitizer = new Sanitizer($givenData, $expectedData);

        $sanitizedData = $sanitizer->sanitize();

        $this->assertSame($sanitizedData, $expectedReturn);
    }
}
