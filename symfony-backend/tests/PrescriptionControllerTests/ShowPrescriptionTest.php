<?php

namespace App\Tests\PrescriptionControllerTests;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class ShowPrescriptionTest extends WebTestCase
{
    public function setUp()
    {
        $this->client = static::createClient();
    }

    public function testLoginAsDoctor()
    {
        $doctor = ['username' => 'testymctesterson1990',
                   'password' => 'password'];

        $this->client->request('POST', '/doctors/login', $doctor);
        $response = $this->client->getResponse();

        $this->assertArrayHasKey('authorization', $response->headers->all());
        return $response->headers->get('Authorization');
    }

    /**
     * @depends testLoginAsDoctor
     */
    public function testSuccessfulShowPrescription($authToken)
    {
        $this->client->request(
            'GET',
            '/prescriptions/997a4702-5723-480b-b419-212a09d720cb',
            [],
            [],
            ["HTTP_AUTHORIZATION" => $authToken]
        );

        $response = $this->client->getResponse();
        $content  = json_decode($response->getContent(), true);

        $this->assertSame(200, $response->getStatusCode());
        $this->assertNotSame($authToken, $response->headers->get('Authorization'));

        $this->assertArrayHasKey('patient', $content['data']);
        $this->assertArrayHasKey('id', $content['data']['patient']);
        $this->assertArrayHasKey('firstName', $content['data']['patient']);
        $this->assertArrayHasKey('lastName', $content['data']['patient']);

        $this->assertSame('ae5157e3-a9ea-42c0-aab5-ceaebccba60c', $content['data']['patient']['id']);
        $this->assertSame('admin', $content['data']['patient']['firstName']);
        $this->assertSame('admin', $content['data']['patient']['lastName']);

        $this->assertArrayHasKey('medication', $content['data']);
        $this->assertSame('test medication', $content['data']['medication']);

        return $response->headers->get('Authorization');
    }

    /**
     * @depends testSuccessfulShowPrescription
     */
    public function testUnsuccessfulShowPrescription($authToken)
    {
        $this->client->request(
            'GET',
            '/prescriptions/not-a-real-uuid',
            [],
            [],
            ["HTTP_AUTHORIZATION" => $authToken]
        );

        $response = $this->client->getResponse();
        $content  = json_decode($response->getContent(), true);

        $this->assertSame(400, $response->getStatusCode());
        $this->assertNotSame($authToken, $response->headers->get('Authorization'));

        $this->assertContains('Prescription could not be found', $content['errors']);
    }

    public function testUnauthorizedShowPrescription()
    {
        $this->client->request(
            'GET',
            '/prescriptions/3025be19-4894-4ba7-bb87-daaf0b611a54', // this is the uuid of the admin account
            [],
            [],
            ["HTTP_AUTHORIZATION" => 'not_a_real_auth_token']
        );

        $response = $this->client->getResponse();
        $content  = json_decode($response->getContent(), true);

        $this->assertSame(401, $response->getStatusCode());
    }
}
