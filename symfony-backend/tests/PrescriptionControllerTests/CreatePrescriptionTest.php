<?php

namespace App\Tests\PrescriptionControllerTests;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class CreatePrescriptionTests extends WebTestCase
{
    public function setUp()
    {
        $this->client = static::createClient();
    }

    public function testLoginAsDoctor()
    {
        $doctor = ['username' => 'testymctesterson1990',
                   'password' => 'password'];

        $this->client->request('POST', '/doctors/login', $doctor);
        $response = $this->client->getResponse();

        $this->assertArrayHasKey('authorization', $response->headers->all());
        return $response->headers->get('Authorization');
    }

    /**
     * @depends testLoginAsDoctor
     */
    public function testSuccessfulCreation($authToken)
    {
        $prescriptionInfo = [
            'patient'    => 'ae5157e3-a9ea-42c0-aab5-ceaebccba60c',
            'medication' => 'test medication'
        ];
        $headers = ['HTTP_AUTHORIZATION' => $authToken];

        $this->client->request('POST', '/prescriptions', $prescriptionInfo, [], $headers);
        $response = $this->client->getResponse();

        $this->assertSame($response->getStatusCode(), 201);

        return $response->headers->get('Authorization');
    }

    /**
     * @depends testSuccessfulCreation
     */
    public function testUnsuccessfulCreation($authToken)
    {
        $prescriptionInfo = [];
        $headers = ['HTTP_AUTHORIZATION' => $authToken];

        $this->client->request('POST', '/prescriptions', $prescriptionInfo, [], $headers);
        $response = $this->client->getResponse();
        $content = json_decode($response->getContent(), true);

        $this->assertSame($response->getStatusCode(), 400);
        $this->assertContains('patient is missing', $content['errors']);
        $this->assertContains('medication is missing', $content['errors']);

        return $response->headers->get('Authorization');
    }

    /**
     * @depends testUnsuccessfulCreation
     */
    public function testInvalidAttributes($authToken)
    {
        $prescriptionInfo = [
            'patient'    => 'not a real uuid',
            'medication' => 17
        ];
        $headers = ['HTTP_AUTHORIZATION' => $authToken];

        $this->client->request('POST', '/prescriptions', $prescriptionInfo, [], $headers);
        $response = $this->client->getResponse();
        $content = json_decode($response->getContent(), true);

        $this->assertSame($response->getStatusCode(), 400);
        $this->assertContains('patient could not be found', $content['errors']);
        $this->assertContains('medication is not of type string', $content['errors']);
    }

    public function testUnauthorizedAttempt()
    {
        $this->client->request('POST', '/prescriptions');
        $response = $this->client->getResponse();

        $this->assertSame(401, $response->getStatusCode());
    }
}
