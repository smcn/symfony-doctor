<?php

namespace App\Tests\ReceptionistControllerTests;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class ShowReceptionistTest extends WebTestCase
{
    public function setUp()
    {
        $this->client = static::createClient();
    }

    public function testLoginAsReceptionist()
    {
        $adminReceptionist = ['username' => 'admin',
                              'password' => 'symfony-doctor-admin'];

        $this->client->request('POST', '/receptionists/login', $adminReceptionist);
        $response = $this->client->getResponse();

        $this->assertArrayHasKey('authorization', $response->headers->all());
        return $response->headers->get('Authorization');
    }

    /**
     * @depends testLoginAsReceptionist
     */
    public function testSuccessfulShowReceptionist($authToken)
    {
        $this->client->request(
            'GET',
            '/receptionists/65601fb0-6b17-441f-a3c3-a882ce4cfc7f', // this is the uuid of the admin account
            [],
            [],
            ["HTTP_AUTHORIZATION" => $authToken]
        );

        $response = $this->client->getResponse();
        $content  = json_decode($response->getContent(), true);

        $this->assertSame(200, $response->getStatusCode());
        $this->assertNotSame($authToken, $response->headers->get('Authorization'));

        $this->assertArrayHasKey('firstName', $content['data']);
        $this->assertArrayHasKey('lastName', $content['data']);
        $this->assertArrayHasKey('dateOfBirth', $content['data']);

        $this->assertSame('admin', $content['data']['firstName']);
        $this->assertSame('admin', $content['data']['lastName']);
        $this->assertSame('1700-01-01', $content['data']['dateOfBirth']);

        return $response->headers->get('Authorization');
    }

    /**
     * @depends testSuccessfulShowReceptionist
     */
    public function testUnsuccessfulShowReceptionist($authToken)
    {
        $this->client->request(
            'GET',
            '/receptionists/not-a-real-uuid',
            [],
            [],
            ["HTTP_AUTHORIZATION" => $authToken]
        );

        $response = $this->client->getResponse();
        $content  = json_decode($response->getContent(), true);

        $this->assertSame(400, $response->getStatusCode());
        $this->assertNotSame($authToken, $response->headers->get('Authorization'));

        $this->assertContains('Receptionist could not be found', $content['errors']);
    }

    public function testUnauthorizedShowReceptionist()
    {
        $this->client->request(
            'GET',
            '/receptionists/65601fb0-6b17-441f-a3c3-a882ce4cfc7f', // this is the uuid of the admin account
            [],
            [],
            ["HTTP_AUTHORIZATION" => 'not_a_real_auth_token']
        );

        $response = $this->client->getResponse();
        $content  = json_decode($response->getContent(), true);

        $this->assertSame(401, $response->getStatusCode());
    }
}
