<?php

namespace App\Tests\ReceptionistControllerTests;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class LoginReceptionistTest extends WebTestCase
{
    public function setUp()
    {
        $this->client = static::createClient();
    }

    public function testSuccessfulLogin()
    {
        $adminReceptionist = ['username' => 'admin',
                              'password' => 'symfony-doctor-admin'];

        $this->client->request('POST', '/receptionists/login', $adminReceptionist);
        $response = $this->client->getResponse();
        $content = json_decode($response->getContent(), true);

        $this->assertSame(200, $response->getStatusCode());
        $this->assertArrayHasKey('user', $content['data']);
        $this->assertSame('admin', $content['data']['user']['username']);
        $this->assertSame('admin', $content['data']['user']['firstName']);
        $this->assertSame('admin', $content['data']['user']['lastName']);
        $this->assertSame('65601fb0-6b17-441f-a3c3-a882ce4cfc7f', $content['data']['user']['uuid']);
    }

    public function testNoUsernameOrPassword()
    {
        $this->client->request('POST', '/receptionists/login');
        $response = $this->client->getResponse();
        $content = json_decode($response->getContent(), true);

        $this->assertSame(400, $response->getStatusCode());
        $this->assertContains('The username is missing', $content['errors']);
        $this->assertContains('The password is missing', $content['errors']);
    }

    public function testIncorrectUser()
    {
        $adminReceptionist = ['username' => 'not_an_admin',
                              'password' => 'this is the wrong password'];

        $this->client->request('POST', '/receptionists/login', $adminReceptionist);
        $response = $this->client->getResponse();
        $content = json_decode($response->getContent(), true);

        $this->assertSame(400, $response->getStatusCode());
        $this->assertContains('User not found', $content['errors']);
    }

    public function testIncorrectPassword()
    {
        $adminReceptionist = ['username' => 'admin',
                              'password' => 'this is the wrong password'];

        $this->client->request('POST', '/receptionists/login', $adminReceptionist);
        $response = $this->client->getResponse();
        $content = json_decode($response->getContent(), true);

        $this->assertSame(400, $response->getStatusCode());
        $this->assertContains('Incorrect password', $content['errors'][0]);
    }
}
