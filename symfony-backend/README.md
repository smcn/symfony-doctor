# Symfony Doctor Backend #

This is the backend for Symfony Doctor, a management system for a GPs office, written in PHP using the Symfony framework.

# Documentation #

## How Authentication works ##

When you log in (as any user), you're given a `HTTP_AUTHENTICATION` token in the response headers. This token, if left unused, will expire in 30 minutes. If you do use the token to authenticate an API call, you will be given an updated token.

The token itself is no more than a serialized and base64 encoded PHP array. An example array for a Doctor would look like:

``` php
$token = [
	'time'		=> '2000-12-25',
	'tokenId'	=> '0c187bbe6a84ec63adc5a3a0a9e2b0672ff166a8',
	'userClass' => 'Doctor',
	'userUuid'	=> '85601fh0-3c19-141g-r7c3-a982he4vfn7f'
];
```

Before being returned to you, the token is cached on Redis and so, when you use a token in an API call, it is validated against a cached copy. This means that any tampering with a token will result in a rejected request.

## HTTP Status Codes ##

`200`: Generic everything is OK

`201`: Created something OK

`400`: Bad request

`401`: Unauthorized

`403`: Current user is forbidden

`404`: URL is not a valid route

## HTTP Requests ##

All API responses will return a JSON response.

### Logging In ###

-------------------------------------------------------------------------------

All user collections (`Patients`, `Doctors`, `Receptionists`) can log in the same way.

* **URL**

`/{collection}/login`

* **Method**

`POST`

* **Data Params**

`username='ExamplePatient'`

`password='ExamplePassword'`

* **Success Response**

	* **Code** `200`
	* **Content**

``` json
{'data': {
	'notifications': {'Successfully logged in'},
	'user': {
		'username':  'ExamplePatient00',
		'uuid':      '3130da-312es-3211dw-32123sda',
		'firstName': 'Example',
		'lastName':  'Patient
		}
	}
}
```

* **Error Response**

	* **Code** `400`
	* **Content**
	
``` json
{'errors': {
	'Patient not found', 'Incorrect password'
	}
}
```

### Patients API ###

#### Create Patient ####

-------------------------------------------------------------------------------

* **URL**

`/patients`

* **Method**

`POST`

* **Allowed Users**

`Receptionists`

* **Data Params**

`firstName='Example'`

`lastName='Patient'`

`password='ExamplePassword'`

`dateOfBirth='2000-12-25'`

* **Success Response**

	* **Code** `201`
	* **Content**

``` json
{'data': {
	'username': 'ExamplePatient00',
}
```

* **Error Response**

	* **Code** `400`
	* **Content**
	
``` json
{'errors': {
	'firstName is missing',
	'firstName is not of type string',
	'lastName is missing',
	'lastName is not of type string',
	'password is missing',
	'password is not of type password',
	'dateOfBirth is missing',
	'dateOfBirth is not of type date'
	}
}
```

#### Show Patient ####

-------------------------------------------------------------------------------

* **URL**

`/patients/{uuid}`

* **Method**

`GET`

* **Allowed Users**

`All`

If a `Patient` tries to view another patients profile, it will, instead, return their own.

* **Success Response**

	* **Code** `200`
	* **Content**

``` json
{'data': {
	'3130da-312es-3211dw-32123sda' : {
		'username': 'ExamplePatient00',
		'firstName':   'Example',
		'lastName':    'Patient',
		'dateOfBirth': '2000-12-25'
	}
}
```

* **Error Response**

	* **Code** `400`
	* **Content**
	
``` json
{'errors': {
	'Patient could not be found'
	}
}
```

#### Show All Patients ####

-------------------------------------------------------------------------------

* **URL**

`/patients`

To limit the results to last names that begin with a specific letter, use `/patients?letter=U`

* **Method**

`GET`

* **Allowed Users**

`Receptionists`

* **Success Response**

	* **Code** `200`
	* **Content**

``` json
{'data': {
	'3130da-312es-3211dw-32123sda': 'Example Patient',
	'4161ha-814eh-3712dh-38129ska': 'Another Patient'
	}
}
```

#### Show A Patients Appointments ####

-------------------------------------------------------------------------------

* **URL**

`/patients/{uuid}/appointments`

* **Method**

`GET`

* **Allowed Users**

`Receptionists`, `Patients`

If a `Patient` tries to view another patients appointments, it will, instead, return their own.

* **Success Response**

	* **Code** `200`
	* **Content**
	
``` json
{'data': {
	'3123dsa-3123as-654da-4242dsa': {
		'doctor': {
			'id':        '9889da-985ds-342da-2508da',
			'firstName': 'Example',
			'lastName':  'Doctor'
			},
		'status': 'Pending',
		'time':   '2000-12-25 09:30',
		'note':   'Example note'
		}
	},
	'3127dra-1123ag-852db-4642dya': {
		'doctor': {
			'id':        '1879da-185ds-642dh-2008na',
			'firstName': 'Another',
			'lastName':  'Doctor'
			},
		'status': 'Pending',
		'time':   '2000-12-25 09:45',
		'note':   'Example note'
	}
}
```

* **Error Response**

	* **Code** `400`
	* **Content**
	
``` json
{'errors': {
	'Patient could not be found'
	}
}
```

#### Show A Patients Prescriptions ####

-------------------------------------------------------------------------------

* **URL**

`/patients/{uuid}/prescriptions`

* **Method**

`GET`

* **Allowed Users**

`Doctors`, `Patients`

If a `Patient` tries to view another patients prescriptions, it will, instead, return their own.

* **Success Response**

	* **Code** `200`
	* **Content**
	
``` json
{'data': {
	'3123dsa-3123as-654da-4242dsa': {
		'patient': {
			'id':        '9889da-985ds-342da-2508da',
			'firstName': 'Example',
			'lastName':  'Patient'
			},
		'medication': 'Paracetamol'
		}
	},
	'3127dra-1123ag-852db-4642dya': {
		'patient': {
			'id':        '9889da-985ds-342da-2508da',
			'firstName': 'Example',
			'lastName':  'Patient'
			},
		'medication': 'Ibuprofen'
		}
	}
}
```

* **Error Response**

	* **Code** `400`
	* **Content**
	
``` json
{'errors': {
	'Patient could not be found'
	}
}
```

### Doctors API ###

#### Create Doctor ####

-------------------------------------------------------------------------------

* **URL**

`/doctors`

* **Method**

`POST`

* **Allowed Users**

`Receptionists`

* **Data Params**

`firstName='Example'`

`lastName='Doctor'`

`password='ExamplePassword'`

`dateOfBirth='2000-12-25'`

* **Success Response**

	* **Code** `201`
	* **Content**
	
``` json
{'data': {
	'username': 'ExampleDoctor00'
}
```

* **Error Response**

	* **Code** `400`
	* **Content**
	
``` json
{'errors': {
	'firstName is missing',
	'firstName is not of type string',
	'lastName is missing',
	'lastName is not of type string',
	'password is missing',
	'password is not of type password',
	'dateOfBirth is missing',
	'dateOfBirth is not of type date'
	}
}
```

#### Show Doctor ####

-------------------------------------------------------------------------------

* **URL**

`/doctors/{uuid}`

* **Method**

`GET`

* **Allowed Users**

`All`

* **Success Response**

	* **Code** `200`
	* **Content**
	
``` json
{'data': {
	'firstName':   'Example',
	'lastName:     'Doctor',
	'dateOfBirth': '2000-12-25'
}
```

* **Error Response**

	* **Code** `400`
	* **Content**
	
``` json
{'errors': {
	    'Doctor could not be found'
	}
}
```

#### Show All Doctors ####

-------------------------------------------------------------------------------

* **URL**

`/doctors`

* **Method**

`GET`

To limit the results to last names that begin with a specific letter, use `/doctors?letter=D`

* **Allowed Users**

`Receptionists`, `Patients`

* **Success Response**

	* **Code** `200`
	* **Content**
	
``` json
{'data': {
	'3130da-312es-3211dw-32123sda': 'Example Doctor',
	'4161ha-814eh-3712dh-38129ska': 'Another Doctor'
}
```

#### Show A Doctors Appointments ####

-------------------------------------------------------------------------------

* **URL**

`/doctors/{uuid}/appointments`

* **Method**

`GET`

* **Allowed Users**

`Doctors`, `Receptionists`

If a `Doctor` tries to view another doctors appointments, it will, instead, return their own.

* **Success Response**

	* **Code** `200`
	* **Content**
	
``` json
{'data': {
	'3123dsa-3123as-654da-4242dsa': {
		'patient': {
			'id':        '9889da-985ds-342da-2508da',
			'firstName': 'Example',
			'lastName':  'Patient'
			},
		'status': 'Pending',
		'time':   '2000-12-25 09:30',
		'note':   'Example note'
		}
	},
	'3127dra-1123ag-852db-4642dya': {
		'patient': {
			'id':        '1879da-185ds-642dh-2008na',
			'firstName': 'Another',
			'lastName':  'Patient'
			},
		'status': 'Pending',
		'time':   '2000-12-25 09:45',
		'note':   'Example note'
	}
}
```

### Receptionists API ###

#### Create Receptionist ####

-------------------------------------------------------------------------------

* **URL**

`/receptionists`

* **Method**

`POST`

* **Allowed Users**

`Receptionists`

* **Data Params**

`firstName='Example'`

`lastName='Receptionist'`

`password='ExamplePassword'`

`dateOfBirth='2000-12-25'`

* **Success Response**

	* **Code** `201`
	* **Content**
	
``` json
{'data': {
	'username': 'ExampleReceptionist00'
}
```

* **Error Response**

	* **Code** `400`
	* **Content**
	
``` json
{'errors': {
	'firstName is missing',
	'firstName is not of type string',
	'lastName is missing',
	'lastName is not of type string',
	'password is missing',
	'password is not of type password',
	'dateOfBirth is missing',
	'dateOfBirth is not of type date'
	}
}
```

#### Show Receptionist ####

-------------------------------------------------------------------------------

* **URL**

`/receptionists/{uuid}`

* **Method**

`GET`

* **Allowed Users**

`All`

* **Success Response**

	* **Code** `200`
	* **Content**
	
``` json
{'data': {
	'firstName':   'Example',
	'lastName':    'Receptionist',
	'dateOfBirth': '2000-12-25'
}
```

* **Error Response**

	* **Code** `400`
	* **Content**
	
``` json
{'errors': {
	    'Receptionist could not be found'
	}
}
```

### Appointments API ###

#### Create Appointment ####

-------------------------------------------------------------------------------

* **URL**

`/appointments`

* **Method**

`POST`

* **Allowed Users**

`Patient`, `Receptionists`

If a `Patient` tries to create an appointment for another `Patient`, it will, instead, create one for itself

* **Data Params**

`doctor='3130da-312es-3211dw-32123sda'`

`patient='4161ha-814eh-3712dh-38129ska'`

`time='2000-12-25 9:30'`

`note='Example note'`

* **Success Response**

	* **Code** `201`
	* **Content**
	
``` json
{'data': {
	'Appointment created'
}
```

* **Error Response**

	* **Code** `400`
	* **Content**
	
``` json
{'errors': {
	    'Appointment not created',
		'doctor is missing',
		'doctor could not be found',
		'patient is missing',
		'patient could not be found',
		'time is not of type date',
		'time is missing'
	}
}
```

#### Show Todays Appointments ####

-------------------------------------------------------------------------------
This shows all of the appointments scheduled for today that are `Pending`.

* **URL**

`/appointments/today`

* **Method**

`GET`

* **Allowed Users**

`Receptionists`

* **Success Response**

	* **Code** `200`
	* **Content**
	
``` json
{'data': {
	'312nm-3420dsa-32101iu-3241nmab': {
		'patient': {
			'firstName': 'Example',
			'lastName':  'Patient'
		},
		'doctor': {
			'firstName': 'Example',
			'lastName':  'Doctor'
		},
		'time': '2000-12-25 9:30',
		'note': 'Example note'
	},
	'1231ad-2439csd-43213il-1431dnas': {
		'patient': {
			'firstName': 'Another',
			'lastName':  'Patient'
		},
		'doctor': {
			'firstName': 'Another',
			'lastName':  'Doctor'
		},
		'time': '2000-12-25 9:30',
		'note': 'Example note'
	},
}
```

#### Show Appointment ####

-------------------------------------------------------------------------------

* **URL**

`/appointments/{uuid}`

* **Method**

`GET`

* **Allowed Users**

`All`

* **Success Response**

	* **Code** `200`
	* **Content**
	
``` json
{'data': {
		'patient': {
			'firstName': 'Example',
			'lastName':  'Patient'
		},
		'doctor': {
			'firstName': 'Example',
			'lastName':  'Doctor'
		},
		'status': 'Pending',
		'time':   '2000-12-25 9:30',
		'note':   'Example note'
	}
}
```

* **Error Response**

	* **Code** `400`
	* **Content**
	
``` json
{'errors': {
	    'Appointment could not be found'
	}
}
```

#### Update Appointment ####

-------------------------------------------------------------------------------

* **URL**

`/appointments/{uuid}`

* **Method**

`PUT`

* **Allowed Users**

`Receptionists`

* **Data Params**

`status='Rescheduled'`

* **Success Response**

	* **Code** `201`
	* **Content**
	
``` json
{'data': {
	'Appointment updated'
}
```

* **Error Response**

	* **Code** `400`
	* **Content**
	
``` json
{'errors': {
	    'Appointment could not be found'
	}
}
```

#### Show All Appointments ####

-------------------------------------------------------------------------------

* **URL**

`/appointments`

* **Method**

`GET`

* **Allowed Users**

`Receptionists`

* **Success Response**

	* **Code** `200`
	* **Content**
	
``` json
{'data': {
	'3123dsa-3123as-654da-4242dsa': {
		'patient': {
			'id':        '9889da-985ds-342da-2508da',
			'firstName': 'Example',
			'lastName':  'Patient'
			},
		'status': 'Cancelled',
		'time':   '2000-12-25 09:30',
		'note':   'Example note'
		}
	},
	'3127dra-1123ag-852db-4642dya': {
		'patient': {
			'id':        '1879da-185ds-642dh-2008na',
			'firstName': 'Another',
			'lastName':  'Patient'
			},
		'status': 'Pending',
		'time':   '2000-12-25 09:45',
		'note':   'Example note'
	}
}
```

### Prescriptions API ###

#### Create Prescription ####

-------------------------------------------------------------------------------

* **URL**

`/prescriptions`

* **Method**

`POST`

* **Allowed Users**

`Doctors`

* **Data Params**

`patient='4161ha-814eh-3712dh-38129ska'`

`medication='Paracetamol'`

* **Success Response**

	* **Code** `201`
	* **Content**
	
``` json
{'data': {
	'Prescription created'
}
```

* **Error Response**

	* **Code** `400`
	* **Content**
	
``` json
{'errors': {
	    'Prescription not created',
		'patient is missing',
		'patient could not be found',
		'medication is not of type string',
		'medication is missing'
	}
}
```

#### Show Prescription ####

-------------------------------------------------------------------------------

* **URL**

`/prescriptions/{uuid}`

* **Method**

`GET`

* **Allowed Users**

`Doctors`, `Patients`

* **Success Response**

	* **Code** `200`
	* **Content**
	
``` json
{'data': {
		'patient': {
			'id':        '9889da-985ds-342da-2508da',
			'firstName': 'Example',
			'lastName':  'Patient'
		},
		'medication': 'Paracetamol'
	}
}
```

* **Error Response**

	* **Code** `400`
	* **Content**
	
``` json
{'errors': {
	    'Prescription could not be found'
	}
}
```

